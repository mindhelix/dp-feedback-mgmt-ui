﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.WindowsAzure.StorageClient;

namespace CitizenReports
{
    public class DistrictEntity : TableServiceEntity
    {

        public DistrictEntity(DistrictObject distObj)
        {
            this.PartitionKey = distObj.districtrange;
            this.RowKey = distObj.id.ToString();
            this.entityid = Guid.NewGuid();

            this.id = distObj.id;
            this.name = distObj.name;
            this.districtrange = distObj.districtrange;
        }

        public DistrictEntity() { }

        public Guid entityid { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public string districtrange { get; set; }
    }
}
