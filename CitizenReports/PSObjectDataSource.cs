﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace CitizenReports
{
    public class PSObjectDataSource
    {
        private string tableName = "policestations";
        private string connectionName = "PSConn";
        private static CloudStorageAccount storageAccount;
        private CloudTableClient tableClient;

        public PSObjectDataSource()
        {
            storageAccount = CloudStorageAccount.Parse(RoleEnvironment.GetConfigurationSettingValue(connectionName));
            tableClient = storageAccount.CreateCloudTableClient();
        }


        //Get Police stations with offset and limit
        public IEnumerable<PSObjectEntity> getPoliceStations(int offset = 0, int limit = 10)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var repObjs = (from g in tableServiceContext.CreateQuery<PSObjectEntity>(tableName)
                           select g).AsEnumerable<PSObjectEntity>().Skip(offset).Take(limit);
            return repObjs;
        }

        //Get All Police Stations
        public IEnumerable<PSObjectEntity> getAllPoliceStations()
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var repObjs = (from g in tableServiceContext.CreateQuery<PSObjectEntity>(tableName)
                           select g).AsEnumerable<PSObjectEntity>();
            return repObjs;
        }

        //Get All Police Stations by PS Id
        public IEnumerable<PSObjectEntity> getAllPoliceStationsById(string ps_id)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var repObjs = (from g in tableServiceContext.CreateQuery<PSObjectEntity>(tableName) 
                           where g.RowKey == ps_id 
                           select g).AsEnumerable<PSObjectEntity>();
            return repObjs;
        }

        //Get Police Station from Partition Key and Row Key
        public PSObjectEntity getAPoliceStation(string partitionKey, string rowKey)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();


            PSObjectEntity psObj = new PSObjectEntity();
            psObj = (from g in tableServiceContext.CreateQuery<PSObjectEntity>(tableName) 
                           where g.PartitionKey == partitionKey && g.RowKey == rowKey 
                           select g).FirstOrDefault();

            return psObj;
        }

        
        //Get District_Id from PoliceStation ID -> Row Key
        public int getPSDistrictId(string ps_id)
        {
            try
            {
                TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();


                PSObjectEntity psObj = new PSObjectEntity();
                psObj = (from g in tableServiceContext.CreateQuery<PSObjectEntity>(tableName)
                         where g.RowKey == ps_id
                         select g).FirstOrDefault();

                return psObj.districtid;
            }
            catch
            {
                return 0;
            }
        }


        //Get Police Station from ps_id search
        public IEnumerable<PSObjectEntity> getSearchPS(string ps_id)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var repObjs = (from g in tableServiceContext.CreateQuery<PSObjectEntity>(tableName)
                           where g.RowKey == ps_id 
                           select g).AsEnumerable<PSObjectEntity>();
            return repObjs;
        }


        //Update Police Station
        public void updatePoliceStation(PSObject psObj, string partitionKey, string rowKey)
        {
            PSObjectEntity psEnt = new PSObjectEntity(psObj);
           

            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            PSObjectEntity updatePsEnt = (from g in tableServiceContext.CreateQuery<PSObjectEntity>(tableName)
                                       where g.PartitionKey == partitionKey && g.RowKey == rowKey
                                       select g).FirstOrDefault();

            updatePsEnt.stationname = psEnt.stationname;
            updatePsEnt.shoid = psEnt.shoid;
            updatePsEnt.shophone = psEnt.shophone;
            updatePsEnt.shomobile = psEnt.shomobile;
            updatePsEnt.shoemail = psEnt.shoemail;
            updatePsEnt.latitude = psEnt.latitude;
            updatePsEnt.longitude = psEnt.longitude;
            updatePsEnt.address = psEnt.address;

            tableServiceContext.UpdateObject(updatePsEnt);
            tableServiceContext.SaveChangesWithRetries();
        }


        //Delete Police Station
        public void delPoliceStation(string partitionKey, string rowKey)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            PSObjectEntity delPsEnt = (from g in tableServiceContext.CreateQuery<PSObjectEntity>(tableName)
                                          where g.PartitionKey == partitionKey && g.RowKey == rowKey
                                          select g).FirstOrDefault();

            tableServiceContext.DeleteObject(delPsEnt);
            tableServiceContext.SaveChangesWithRetries();
        }
    }
}
