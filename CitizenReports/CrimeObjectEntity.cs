﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.WindowsAzure.StorageClient;

namespace CitizenReports
{
    public class CrimeObjectEntity : TableServiceEntity
    {
        public CrimeObjectEntity(CrimeObject crimeObj)
        {
            this.PartitionKey = crimeObj.ps_id.ToString();
            this.RowKey = Guid.NewGuid().ToString();
            this.entityid = Guid.NewGuid();

            this.ps_id = crimeObj.ps_id;
            this.incident_count = crimeObj.incident_count;
            this.date = crimeObj.date;
            this.district_id = crimeObj.district_id;
            this.crime_head = crimeObj.crime_head;
        }

        public CrimeObjectEntity() { }

        public Guid entityid { get; set; }
        public int ps_id { get; set; }
        public int incident_count { get; set; }
        public DateTime date { get; set; }
        public int district_id { get; set; }
        public string crime_head { get; set; }
    }
}
