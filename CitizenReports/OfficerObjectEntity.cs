﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.WindowsAzure.StorageClient;

namespace CitizenReports
{
    public class OfficerObjectEntity : TableServiceEntity
    {
        public OfficerObjectEntity(OfficerObject officer)
        {
            this.PartitionKey = Guid.NewGuid().ToString();
            this.RowKey = officer.id.ToString();
            this.entityid = Guid.Parse(this.PartitionKey);
            this.id = officer.id;
            this.name = officer.name;
        }

        public OfficerObjectEntity() { }

        public Guid entityid { get; set; }
        public string id { get; set; }
        public string name { get; set; }
    }
}
