﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace CitizenReports
{
    public class UserDataDataSource
    {
        private string tableName = "UserDataTable";
        private string connectionName = "ReportsConn";
        private static CloudStorageAccount storageAccount;
        private CloudTableClient tableClient;
        private TableServiceContext tableServiceContext;

        public UserDataDataSource()
        {
            storageAccount = CloudStorageAccount.Parse(RoleEnvironment.GetConfigurationSettingValue(connectionName));
            tableClient = storageAccount.CreateCloudTableClient();
            tableServiceContext = tableClient.GetDataServiceContext();
        }


        //Add UserData
        public void AddUserData(UserDataObject userData)
        {
            UserDataEntity userDataEnt = new UserDataEntity(userData);

            tableClient.CreateTableIfNotExist(tableName);
            tableServiceContext.AddObject(tableName, userDataEnt);
            tableServiceContext.SaveChangesWithRetries();
        }

        
        //get UserData
        public UserDataEntity GetUserData(string user_id, string data_key)
        {
            UserDataEntity userObj = (from g in tableServiceContext.CreateQuery<UserDataEntity>(tableName)
                           where g.PartitionKey == user_id && g.RowKey == data_key
                           select g).FirstOrDefault();

            return userObj;
        }


        //Update UserData
        public void UpdateUserData(string user_id, string data_key, string new_data_value)
        {
            UserDataEntity userObj = (from g in tableServiceContext.CreateQuery<UserDataEntity>(tableName) 
                                      where g.PartitionKey == user_id && g.RowKey == data_key 
                                      select g).FirstOrDefault();

            userObj.data_value = new_data_value;
            userObj.date_time = DateTime.UtcNow;

            tableServiceContext.UpdateObject(userObj);
            tableServiceContext.SaveChangesWithRetries();
        }


        //Delete All UserData
        //->user_id = PartitionKey
        public void DelAllUserData(string user_id)
        {
            UserDataEntity userDelEnt = (from g in tableServiceContext.CreateQuery<UserDataEntity>(tableName)
                                         where g.PartitionKey == user_id 
                                         select g).FirstOrDefault();

            tableServiceContext.DeleteObject(userDelEnt);
            tableServiceContext.SaveChangesWithRetries();
        }
    }
}
