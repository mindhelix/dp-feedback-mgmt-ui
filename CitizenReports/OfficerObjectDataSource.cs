﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace CitizenReports
{
    public class OfficerObjectDataSource
    {
        private string tableName = "Officers";
        private string connectionName = "PSConn";
        private static CloudStorageAccount  storageAccount;
        private CloudTableClient tableClient;

        public OfficerObjectDataSource()
        {
            storageAccount = CloudStorageAccount.Parse(RoleEnvironment.GetConfigurationSettingValue(connectionName));
            tableClient = storageAccount.CreateCloudTableClient();
        }


        //Put Officer
        public void putOfficer(OfficerObject officerObj)
        {
            OfficerObjectEntity officerEnt = new OfficerObjectEntity(officerObj);

            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            tableClient.CreateTableIfNotExist(tableName);

            tableServiceContext.AddObject(tableName, officerEnt);
            tableServiceContext.SaveChangesWithRetries();
        }


        //Get Officers with offset and limit
        public IEnumerable<OfficerObjectEntity> getOfficers(int offset = 0, int limit = 10)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var officerEnts = (from g in tableServiceContext.CreateQuery<OfficerObjectEntity>(tableName) 
                           select g).AsEnumerable<OfficerObjectEntity>().OrderBy(x=>x.RowKey).Skip(offset).Take(limit);
            return officerEnts;
        }


        //Get All Officers
        public IEnumerable<OfficerObjectEntity> getAllOfficers()
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var officerEnts = (from g in tableServiceContext.CreateQuery<OfficerObjectEntity>(tableName)
                               select g).AsEnumerable<OfficerObjectEntity>();
            return officerEnts;
        }


        //Get A Officer
        public IEnumerable<OfficerObjectEntity> getAOfficer(string sho_id, string partitonKey)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var officerEnt = (from g in tableServiceContext.CreateQuery<OfficerObjectEntity>(tableName) 
                             where g.PartitionKey == partitonKey && g.RowKey == sho_id 
                             select g).AsEnumerable<OfficerObjectEntity>();
            return officerEnt;
        }


        //Get A Officer By ID
        public IEnumerable<OfficerObjectEntity> getOfficerById(string sho_id)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var officerEnt = (from g in tableServiceContext.CreateQuery<OfficerObjectEntity>(tableName)
                              where g.RowKey == sho_id
                              select g).AsEnumerable<OfficerObjectEntity>();
            return officerEnt;
        }


        //Check Officer ID
        public bool chkOfficerID(string sho_id, string new_sho_id)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var officerEnt = (from g in tableServiceContext.CreateQuery<OfficerObjectEntity>(tableName) 
                              where g.RowKey != sho_id && g.RowKey == new_sho_id 
                              select g).AsEnumerable<OfficerObjectEntity>().Count();

            if (officerEnt != 0)
                return true;
            else
                return false;
        }

        //Update A Officer
        public void updateOfficer(string sho_id, string partitonKey, OfficerObject officerObj)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var updateOfficerEnt = (from g in tableServiceContext.CreateQuery<OfficerObjectEntity>(tableName)
                              where g.PartitionKey == partitonKey && g.RowKey == sho_id
                              select g).FirstOrDefault();


            OfficerObjectEntity officerEnt = new OfficerObjectEntity(officerObj);
            officerEnt.PartitionKey = updateOfficerEnt.PartitionKey;
            officerEnt.entityid = updateOfficerEnt.entityid;

            //delete current entity
            tableServiceContext.DeleteObject(updateOfficerEnt);
            tableServiceContext.SaveChangesWithRetries();

            //add as new Entity
            tableClient.CreateTableIfNotExist(tableName);
            tableServiceContext.AddObject(tableName, officerEnt);
            tableServiceContext.SaveChangesWithRetries();
        }

        
        //Delete SHO
        public void deleteSho(string sho_id, string pk)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var deleteEnt = (from g in tableServiceContext.CreateQuery<OfficerObjectEntity>(tableName)
                             where g.PartitionKey == pk && g.RowKey == sho_id
                             select g).FirstOrDefault();

            tableServiceContext.DeleteObject(deleteEnt);
            tableServiceContext.SaveChangesWithRetries();
        }

        
        //Blob Operations
        

        //put SHO Photo
        public void putShoPhoto(string sho_id, Stream photoFileStream)
        {
            storageAccount = CloudStorageAccount.Parse(RoleEnvironment.GetConfigurationSettingValue(connectionName));
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            //get Blob container
            CloudBlobContainer blobContainer = blobClient.GetContainerReference("sho-photos");
            blobContainer.CreateIfNotExist();

            //get Blob reference
            CloudBlob blob = blobContainer.GetBlobReference(string.Format("{0}.jpg", sho_id));

            //upload blob
            blob.UploadFromStream(photoFileStream);
        }


        public void deleteShoPhoto(string sho_id)
        {
            storageAccount = CloudStorageAccount.Parse(RoleEnvironment.GetConfigurationSettingValue(connectionName));
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            //get Blob container
            CloudBlobContainer blobContainer = blobClient.GetContainerReference("sho-photos");
            blobContainer.CreateIfNotExist();

            //get Blob reference
            CloudBlob blob = blobContainer.GetBlobReference(string.Format("{0}.jpg", sho_id));

            blob.DeleteIfExists();
        }


        //get SHO Photos
        public CloudBlobContainer getShoPhotos()
        {
            storageAccount = CloudStorageAccount.Parse(RoleEnvironment.GetConfigurationSettingValue(connectionName));
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            //get Blob container
            CloudBlobContainer blobContainer = blobClient.GetContainerReference("sho-photos");

            return blobContainer;
        }


        //get single SHO Photo
        public CloudBlob getAShoPhoto(string sho_id)
        {
            storageAccount = CloudStorageAccount.Parse(RoleEnvironment.GetConfigurationSettingValue(connectionName));
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();


            //get Blob container
            CloudBlobContainer blobContainer = blobClient.GetContainerReference("sho-photos");

            //get Blob
            CloudBlob blob = blobContainer.GetBlobReference(string.Format("{0}.jpg", sho_id));

            return blob;
        }


        //update Blob ID
        public void updateShoPhotoID(string sho_id, string new_sho_id)
        {
            //if same, don't update
            if (sho_id == new_sho_id)
                return;

            storageAccount = CloudStorageAccount.Parse(RoleEnvironment.GetConfigurationSettingValue(connectionName));
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            //get Blob container
            CloudBlobContainer blobContainer = blobClient.GetContainerReference("sho-photos");

            //get current Blob
            CloudBlob blob = blobContainer.GetBlobReference(string.Format("{0}.jpg", sho_id));
            
            //create new blob
            CloudBlob newBlob = blobContainer.GetBlobReference(string.Format("{0}.jpg", new_sho_id));
            newBlob.CopyFromBlob(blob);

            //delete old
            blob.Delete();
        }
    }
}
