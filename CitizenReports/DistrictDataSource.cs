﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace CitizenReports
{
    public class DistrictDataSource
    {
        private string tableName = "PoliceDistricts";
        private string connectionName = "PSConn";
        private static CloudStorageAccount storageAccount;
        private CloudTableClient tableClient;

        public DistrictDataSource()
        {
            storageAccount = CloudStorageAccount.Parse(RoleEnvironment.GetConfigurationSettingValue(connectionName));
            tableClient = storageAccount.CreateCloudTableClient();
        }

        //Get A District from District ID -> RowKey
        public IEnumerable<DistrictEntity> getADistrict(string rk)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var distEnt = (from g in tableServiceContext.CreateQuery<DistrictEntity>(tableName)
                           where g.RowKey == rk
                           select g).AsEnumerable<DistrictEntity>();
            return distEnt;
        }


        //Get All Districts
        public IEnumerable<DistrictEntity> getAllDistricts()
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var distEnts = (from g in tableServiceContext.CreateQuery<DistrictEntity>(tableName) 
                           select g).AsEnumerable<DistrictEntity>();

            return distEnts;
        }


        //Get All Ranges
        public IEnumerable<DistrictEntity> getAllRanges()
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var distEnts = (from g in tableServiceContext.CreateQuery<DistrictEntity>(tableName)
                            select g).AsEnumerable<DistrictEntity>().GroupBy(x => x.districtrange).Select(x => x.First());

            return distEnts;
        }


        //Get Range from District_Id
        public string getRangeByDsId(int district_id)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var distEnts = (from g in tableServiceContext.CreateQuery<DistrictEntity>(tableName)
                            where g.RowKey == district_id.ToString()  
                            select g).AsEnumerable<DistrictEntity>().FirstOrDefault();

            return distEnts.PartitionKey;
        }
    }
}
