﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.WindowsAzure.StorageClient;

namespace CitizenReports
{
    public class UserDataEntity : TableServiceEntity
    {
        public UserDataEntity(UserDataObject userDataObj)
        {
            this.PartitionKey = userDataObj.user_id;
            this.RowKey = userDataObj.data_key;

            this.entityid = DateTime.UtcNow.Ticks.ToString() + "_" + Guid.NewGuid().ToString();
            this.user_id = userDataObj.user_id;
            this.data_key = userDataObj.data_key;
            this.data_value = userDataObj.data_value;
            this.date_time = DateTime.UtcNow;
        }

        public UserDataEntity() { }

        public string entityid { get; set; }
        public string user_id { get; set; }
        public string data_key { get; set; }
        public string data_value { get; set; }
        public DateTime date_time { get; set; }
    }
}
