﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace CitizenReports
{
    public class UserObjectDataSource
    {
        private string tableName = "UserTable";
        private string connectionName = "ReportsConn";
        private static CloudStorageAccount storageAccount;
        private CloudTableClient tableClient;

        public UserObjectDataSource()
        {
            storageAccount = CloudStorageAccount.Parse(RoleEnvironment.GetConfigurationSettingValue(connectionName));
            tableClient = storageAccount.CreateCloudTableClient();
        }

        public UserObjectEntity CreateUser(UserObject user)
        {
            UserObjectEntity userEntity = new UserObjectEntity(user);

            tableClient.CreateTableIfNotExist(tableName);
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();
            tableServiceContext.AddObject(tableName, userEntity);
            tableServiceContext.SaveChangesWithRetries();

            return userEntity;
        }


        //Update Password from user_id -> RowKey
        public void UpdatePassword(string user_id, string new_pass)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            UserObjectEntity userEnt = (from g in tableServiceContext.CreateQuery<UserObjectEntity>(tableName)
                            where g.RowKey == user_id
                            select g).FirstOrDefault();

            userEnt.Password = new_pass;

            tableServiceContext.UpdateObject(userEnt);
            tableServiceContext.SaveChangesWithRetries();
        }


        //This function is for login check
        public UserObjectEntity checkUser(string username, string password)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var userObj = (from g in tableServiceContext.CreateQuery<UserObjectEntity>(tableName) 
                           where g.Username == username && g.Password == password 
                           select g).FirstOrDefault();

            return userObj;
        }


        //Get Username from user_id -> RowKey
        public string getUsername(string user_id)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var userObj = (from g in tableServiceContext.CreateQuery<UserObjectEntity>(tableName)
                           where g.RowKey == user_id
                           select g).FirstOrDefault();

            return userObj.Username;
        }

        //Check existing user
        public bool doesUserExists(string username)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var userObjs = (from g in tableServiceContext.CreateQuery<UserObjectEntity>(tableName)
                            where g.Username == username 
                            select g).FirstOrDefault();
            if (userObjs != null)
                return true;
            else
                return false;
        }


        //Get User
        public IEnumerable<UserObjectEntity> GetUsersByLevel(int level)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var userEnts = (from g in tableServiceContext.CreateQuery<UserObjectEntity>(tableName)
                           select g).AsEnumerable<UserObjectEntity>();

            var filteredUsers = new List<UserObjectEntity>();

            foreach (UserObjectEntity userEnt in userEnts)
            {
                if (userEnt.Level == level)
                    filteredUsers.Add(userEnt);
            }

            return filteredUsers;
        }


        //Remove a user
        //->user_id -> RowKey
        public void DelUser(string user_id)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            UserObjectEntity delUserEnt = (from g in tableServiceContext.CreateQuery<UserObjectEntity>(tableName)
                                          where g.RowKey == user_id
                                          select g).FirstOrDefault();

            tableServiceContext.DeleteObject(delUserEnt);
            tableServiceContext.SaveChangesWithRetries();
        }
    }
}
