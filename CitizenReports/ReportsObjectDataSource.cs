﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.StorageClient;

namespace CitizenReports
{
    public class ReportsObjectDataSource
    {
        private string tableName = "CitizenReport";
        private string connectionName = "ReportsConn";
        private static CloudStorageAccount storageAccount;
        private CloudTableClient tableClient;

        public ReportsObjectDataSource()
        {
            storageAccount = CloudStorageAccount.Parse(RoleEnvironment.GetConfigurationSettingValue(connectionName));
            tableClient = storageAccount.CreateCloudTableClient();
        }


        public void postFeedback(ReportObject repObj)
        {
            ReportsObjectEntity repDS = new ReportsObjectEntity(repObj);

            tableClient.CreateTableIfNotExist(tableName);
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();
            tableServiceContext.AddObject(tableName, repDS);
            tableServiceContext.SaveChangesWithRetries();
        }

        //Get Feedback with offset and limit
        public IEnumerable<ReportsObjectEntity> getFeedbacks(int offset = 0, int limit = 10)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                          select g).AsEnumerable<ReportsObjectEntity>().OrderByDescending(x=>x.date_time).Skip(offset).Take(limit);

            return repObjs;
        }


        //Get Feedback with offset and limit with District_Id
        public IEnumerable<ReportsObjectEntity> getFeedbacksWithDS(int district_id, int offset = 0, int limit = 10)
        {
            PSObjectDataSource psDS = new PSObjectDataSource();
            int repDsId = 0;

            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>();

            var filtered = new List<ReportsObjectEntity>();

            foreach (ReportsObjectEntity repEnt in repObjs)
            {
                repDsId = psDS.getPSDistrictId(repEnt.PartitionKey);
                if (repDsId == district_id)
                    filtered.Add(repEnt);
            }

            return filtered.OrderByDescending(x => x.date_time).Skip(offset).Take(limit);
        }

        //Get Feedback with offset and limit and with Range
        public IEnumerable<ReportsObjectEntity> getFeedbacksWithRange(string range, int offset = 0, int limit = 10)
        {
            PSObjectDataSource psDS = new PSObjectDataSource();
            DistrictDataSource distDS = new DistrictDataSource();
            int repDsId = 0;
            string repDSRange = "";

            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>();

            var filtered = new List<ReportsObjectEntity>();

            foreach (ReportsObjectEntity repEnt in repObjs)
            {
                repDsId = psDS.getPSDistrictId(repEnt.PartitionKey);
                repDSRange = distDS.getRangeByDsId(repDsId);
                if (range == repDSRange)
                    filtered.Add(repEnt);
            }

            return filtered.OrderByDescending(x => x.date_time).Skip(offset).Take(limit);
        }


        //Get A Feedback with PK and RK
        public IEnumerable<ReportsObjectEntity> getAFeedback(string pk, string rk)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName) 
                           where g.PartitionKey == pk && g.RowKey == rk 
                           select g).AsEnumerable<ReportsObjectEntity>();

            return repObjs;
        }

        //Get All Feedbacks
        public IEnumerable<ReportsObjectEntity> getAllFeedbacks()
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().OrderByDescending(x => x.date_time);
            return repObjs;
        }


        //Get All Feedbacks with DS
        public IEnumerable<ReportsObjectEntity> getAllFeedbacksWithDS(int district_id)
        {
            PSObjectDataSource psDS = new PSObjectDataSource();
            int repDsId = 0;

            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName) 
                           select g).AsEnumerable<ReportsObjectEntity>();

            var filtered = new List<ReportsObjectEntity>();

            foreach (ReportsObjectEntity repEnt in repObjs)
            {
                repDsId = psDS.getPSDistrictId(repEnt.PartitionKey);
                if (repDsId == district_id)
                    filtered.Add(repEnt);
            }

            return filtered.OrderByDescending(x => x.date_time);
        }

        //Get All Feedbacks with Range
        public IEnumerable<ReportsObjectEntity> getAllFeedbacksWithRange(string range)
        {
            PSObjectDataSource psDS = new PSObjectDataSource();
            DistrictDataSource distDS = new DistrictDataSource();
            int repDsId = 0;
            string repDSRange = "";

            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>();

            var filtered = new List<ReportsObjectEntity>();

            foreach (ReportsObjectEntity repEnt in repObjs)
            {
                repDsId = psDS.getPSDistrictId(repEnt.PartitionKey);
                repDSRange = distDS.getRangeByDsId(repDsId);
                if (range == repDSRange)
                    filtered.Add(repEnt);
            }

            return filtered.OrderByDescending(x => x.date_time);
        }

        //Get Filtered Feedback with offset and limit
        public IEnumerable<ReportsObjectEntity> getFilteredFeedbacks(string ps_name, string search_type = "0", int offset = 0, int limit = 10)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            IEnumerable<ReportsObjectEntity> repObjs = null;
            
            if (search_type == "0")
            {
                repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().Where(x => x.policestation == ps_name);
            }
            else if (search_type == "1") //Unread
            {
                repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().Where(x => (x.policestation == ps_name) && x.approval_stat == false);
            }
            else //Read
            {
                repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().Where(x => (x.policestation == ps_name) && x.approval_stat == true);
            }

            return repObjs.OrderByDescending(x => x.date_time).Skip(offset).Take(limit);
        }


        //Get Filtered Feedback with offset and limit and with District Id
        public IEnumerable<ReportsObjectEntity> getFilteredFeedbacksWithDS(int district_id, string ps_name, string search_type = "0", int offset = 0, int limit = 10)
        {
            PSObjectDataSource psDS = new PSObjectDataSource();
            int repDsId = 0;

            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            IEnumerable<ReportsObjectEntity> repObjs;
            if (search_type == "0")
            {
                repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().Where(x => x.policestation == ps_name);
            }
            else if (search_type == "1") //Unread
            {
                repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().Where(x => (x.policestation == ps_name) && x.approval_stat == false);
            }
            else //Read
            {
                repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().Where(x => (x.policestation == ps_name) && x.approval_stat == true);
            }

            var filtered = new List<ReportsObjectEntity>();

            foreach (ReportsObjectEntity repEnt in repObjs)
            {
                repDsId = psDS.getPSDistrictId(repEnt.PartitionKey);
                if (repDsId == district_id)
                    filtered.Add(repEnt);
            }

            return filtered.OrderByDescending(x => x.date_time).Skip(offset).Take(limit);
        }


        //Get Filtered Feedback with offset and limit and with Range
        public IEnumerable<ReportsObjectEntity> getFilteredFeedbacksWithRange(string range, string ps_name, string search_type = "0", int offset = 0, int limit = 10)
        {
            PSObjectDataSource psDS = new PSObjectDataSource();
            DistrictDataSource distDS = new DistrictDataSource();
            int repDsId = 0;
            string repDsRange = "";

            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            IEnumerable<ReportsObjectEntity> repObjs;
            if (search_type == "0")
            {
                repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().Where(x => x.policestation == ps_name);
            }
            else if (search_type == "1") //Unread
            {
                repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().Where(x => (x.policestation == ps_name) && x.approval_stat == false);
            }
            else //Read
            {
                repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().Where(x => (x.policestation == ps_name) && x.approval_stat == true);
            }

            var filtered = new List<ReportsObjectEntity>();

            foreach (ReportsObjectEntity repEnt in repObjs)
            {
                repDsId = psDS.getPSDistrictId(repEnt.PartitionKey);
                repDsRange = distDS.getRangeByDsId(repDsId);
                if (range == repDsRange)
                    filtered.Add(repEnt);
            }

            return filtered.OrderByDescending(x => x.date_time).Skip(offset).Take(limit);
        }

        //Get All Filtered Feedbacks 
        public IEnumerable<ReportsObjectEntity> getAllFilteredFeedbacks(string ps_name, string search_type = "0")
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            IEnumerable<ReportsObjectEntity> repObjs;
            if (search_type == "0")
            {
                repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().Where(x => x.policestation == ps_name).
                                                                        OrderByDescending(x => x.date_time);
            }
            else if (search_type == "1") //Unread
            {
                repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().Where(x => (x.policestation == ps_name) && x.approval_stat == false).
                                                                        OrderByDescending(x => x.date_time);
            }
            else //Read
            {
                repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().Where(x => (x.policestation == ps_name) && x.approval_stat == true).
                                                                        OrderByDescending(x => x.date_time);
            }

            return repObjs;
        }


        //Get All Filtered Feedbacks with DS 
        public IEnumerable<ReportsObjectEntity> getAllFilteredFeedbacksWithDS(int district_id, string ps_name, string search_type = "0")
        {
            PSObjectDataSource psDS = new PSObjectDataSource();
            int repDsId = 0;

            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            IEnumerable<ReportsObjectEntity> repObjs;
            if (search_type == "0")
            {
                repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().Where(x => x.policestation == ps_name);
            }
            else if (search_type == "1") //Unread
            {
                repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().Where(x => (x.policestation == ps_name) && x.approval_stat == false);
            }
            else //Read
            {
                repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().Where(x => (x.policestation == ps_name) && x.approval_stat == true);
            }

            var filtered = new List<ReportsObjectEntity>();

            foreach (ReportsObjectEntity repEnt in repObjs)
            {
                repDsId = psDS.getPSDistrictId(repEnt.PartitionKey);
                if (repDsId == district_id)
                    filtered.Add(repEnt);
            }

            return filtered.OrderByDescending(x => x.date_time);
        }


        //Get All Filtered Feedbacks with Range 
        public IEnumerable<ReportsObjectEntity> getAllFilteredFeedbacksWithRange(string range, string ps_name, string search_type = "0")
        {
            PSObjectDataSource psDS = new PSObjectDataSource();
            DistrictDataSource distDS = new DistrictDataSource();
            int repDsId = 0;
            string repDsRange = "";

            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            IEnumerable<ReportsObjectEntity> repObjs;
            if (search_type == "0")
            {
                repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().Where(x => x.policestation == ps_name);
            }
            else if (search_type == "1") //Unread
            {
                repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().Where(x => (x.policestation == ps_name) && x.approval_stat == false);
            }
            else //Read
            {
                repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().Where(x => (x.policestation == ps_name) && x.approval_stat == true);
            }

            var filtered = new List<ReportsObjectEntity>();

            foreach (ReportsObjectEntity repEnt in repObjs)
            {
                repDsId = psDS.getPSDistrictId(repEnt.PartitionKey);
                repDsRange = distDS.getRangeByDsId(repDsId);
                if (range == repDsRange)
                    filtered.Add(repEnt);
            }

            return filtered.OrderByDescending(x => x.date_time);
        }


        //Get All PS
        public IEnumerable<ReportsObjectEntity> getAllPoliceStations()
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().GroupBy(x=>x.policestation).Select(x=>x.First());
            return repObjs;
        }


        //Get All PS with DS
        public IEnumerable<ReportsObjectEntity> getAllPoliceStationsWithDS(int district_id)
        {
            PSObjectDataSource psDS = new PSObjectDataSource();
            int repDsId = 0;

            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>();

            var filtered = new List<ReportsObjectEntity>();

            foreach (ReportsObjectEntity repEnt in repObjs)
            {
                repDsId = psDS.getPSDistrictId(repEnt.PartitionKey);
                if (repDsId == district_id)
                    filtered.Add(repEnt);
            }

            return filtered.GroupBy(x => x.policestation).Select(x => x.First());
        }

        //Get All PS with Range
        public IEnumerable<ReportsObjectEntity> getAllPoliceStationsWithRange(string range)
        {
            PSObjectDataSource psDS = new PSObjectDataSource();
            DistrictDataSource distDS = new DistrictDataSource();
            int repDsId = 0;
            string repDsRange = "";

            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>();

            var filtered = new List<ReportsObjectEntity>();

            foreach (ReportsObjectEntity repEnt in repObjs)
            {
                repDsId = psDS.getPSDistrictId(repEnt.PartitionKey);
                repDsRange = distDS.getRangeByDsId(repDsId);
                if (range == repDsRange)
                    filtered.Add(repEnt);
            }

            return filtered.GroupBy(x => x.policestation).Select(x => x.First());
        }

        //Get Feedback based on search
        public IEnumerable<ReportsObjectEntity> getSearchedFeedbacks(string ps_name, string search_type = "0")
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            IEnumerable<ReportsObjectEntity> repObjs;
            if (search_type == "0")
                repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().Where(x => x.policestation == ps_name);
            else if(search_type == "1")
                repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName) 
                           select g).AsEnumerable<ReportsObjectEntity>().Where(x => (x.policestation == ps_name) && x.approval_stat == false);
            else
                repObjs = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                           select g).AsEnumerable<ReportsObjectEntity>().Where(x => (x.policestation == ps_name) && x.approval_stat == true);
            
            return repObjs;
        }


        //Update Approval Stat
        public void updateApproval(string ps_id, string rowkey, bool stat)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            ReportsObjectEntity updateRepEnt = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                                            where g.PartitionKey == ps_id
                                            && g.RowKey == rowkey
                                            select g).FirstOrDefault();

            
            //New repEnt
            ReportsObjectEntity repEnt = new ReportsObjectEntity();
            //Re-assign old values
            repEnt.PartitionKey = updateRepEnt.PartitionKey;
            repEnt.RowKey = updateRepEnt.RowKey;
            repEnt.Timestamp = updateRepEnt.Timestamp;
            repEnt.policestation = updateRepEnt.policestation;
            repEnt.name = updateRepEnt.name;
            repEnt.phone = updateRepEnt.phone;
            repEnt.email = updateRepEnt.email;
            repEnt.details = updateRepEnt.details;
            repEnt.date_time = updateRepEnt.date_time;
            repEnt.sho_reply = updateRepEnt.sho_reply;
            if (repEnt.sho_reply == null)
                repEnt.sho_reply_time = DateTime.UtcNow;
            else
                repEnt.sho_reply_time = updateRepEnt.sho_reply_time;

            repEnt.dcp_reply = updateRepEnt.dcp_reply;
            repEnt.dcp_reply_public = updateRepEnt.dcp_reply_public;
            if (repEnt.dcp_reply == null)
                repEnt.dcp_reply_time = DateTime.UtcNow;
            else
                repEnt.dcp_reply_time = updateRepEnt.dcp_reply_time;

            repEnt.dcp_non_public_reply = updateRepEnt.dcp_non_public_reply;
            if (repEnt.dcp_non_public_reply == null)
                repEnt.dcp_non_public_reply_time = DateTime.UtcNow;
            else
                repEnt.dcp_non_public_reply_time = updateRepEnt.dcp_non_public_reply_time;

            repEnt.sms_sho_phone = updateRepEnt.sms_sho_phone;
            if (repEnt.sms_send_time.HasValue)
                repEnt.sms_send_time = updateRepEnt.sms_send_time.Value;
            repEnt.sms_sent = updateRepEnt.sms_sent;

            repEnt.category = updateRepEnt.category;
            repEnt.imageid = updateRepEnt.imageid;

            //New Values
            repEnt.approval_stat = stat;
            repEnt.read_time = DateTime.UtcNow;

            //Delete old entry
            tableServiceContext.DeleteObject(updateRepEnt);
            tableServiceContext.SaveChangesWithRetries();

            //Add new Entity
            tableServiceContext.AddObject(tableName, repEnt);
            tableServiceContext.SaveChangesWithRetries();
        }


        //Update DCP's reply
        public void updateDcpReply(string ps_id, string rowkey, string dcp_reply, bool reply_public)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            ReportsObjectEntity updateRepEnt = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                                          where g.PartitionKey == ps_id
                                          && g.RowKey == rowkey
                                          select g).FirstOrDefault();

            //New repEnt
            ReportsObjectEntity repEnt = new ReportsObjectEntity();
            //Re-assign old values
            repEnt.PartitionKey = updateRepEnt.PartitionKey;
            repEnt.RowKey = updateRepEnt.RowKey;
            repEnt.Timestamp = updateRepEnt.Timestamp;
            repEnt.policestation = updateRepEnt.policestation;
            repEnt.name = updateRepEnt.name;
            repEnt.phone = updateRepEnt.phone;
            repEnt.email = updateRepEnt.email;
            repEnt.details = updateRepEnt.details;
            repEnt.date_time = updateRepEnt.date_time;
            repEnt.approval_stat = updateRepEnt.approval_stat;
            if (repEnt.approval_stat == false)
                repEnt.read_time = DateTime.UtcNow;
            else
                repEnt.read_time = updateRepEnt.read_time;
            repEnt.sho_reply = updateRepEnt.sho_reply;
            if (repEnt.sho_reply == null)
                repEnt.sho_reply_time = DateTime.UtcNow;
            else
                repEnt.sho_reply_time = updateRepEnt.sho_reply_time;

            repEnt.dcp_non_public_reply = updateRepEnt.dcp_non_public_reply;
            if (repEnt.dcp_non_public_reply == null)
                repEnt.dcp_non_public_reply_time = DateTime.UtcNow;
            else
                repEnt.dcp_non_public_reply_time = updateRepEnt.dcp_non_public_reply_time;

            repEnt.sms_sho_phone = updateRepEnt.sms_sho_phone;
            if (repEnt.sms_send_time.HasValue)
                repEnt.sms_send_time = updateRepEnt.sms_send_time.Value;
            repEnt.sms_sent = updateRepEnt.sms_sent;

            repEnt.category = updateRepEnt.category;
            repEnt.imageid = updateRepEnt.imageid;

            //Assign new values
            repEnt.dcp_reply = dcp_reply;
            repEnt.dcp_reply_public = reply_public;
            repEnt.dcp_reply_time = DateTime.UtcNow;

            //Delete old entry
            tableServiceContext.DeleteObject(updateRepEnt);
            tableServiceContext.SaveChangesWithRetries();

            //Add new Entity
            tableServiceContext.AddObject(tableName, repEnt);
            tableServiceContext.SaveChangesWithRetries();
        }


        //Update DCP's Non Public reply
        public void updateDcpNPReply(string ps_id, string rowkey, string dcp_non_public_reply)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            ReportsObjectEntity updateRepEnt = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                                                where g.PartitionKey == ps_id
                                                && g.RowKey == rowkey
                                                select g).FirstOrDefault();

            //New repEnt
            ReportsObjectEntity repEnt = new ReportsObjectEntity();
            //Re-assign old values
            repEnt.PartitionKey = updateRepEnt.PartitionKey;
            repEnt.RowKey = updateRepEnt.RowKey;
            repEnt.Timestamp = updateRepEnt.Timestamp;
            repEnt.policestation = updateRepEnt.policestation;
            repEnt.name = updateRepEnt.name;
            repEnt.phone = updateRepEnt.phone;
            repEnt.email = updateRepEnt.email;
            repEnt.details = updateRepEnt.details;
            repEnt.date_time = updateRepEnt.date_time;
            repEnt.approval_stat = updateRepEnt.approval_stat;
            if (repEnt.approval_stat == false)
                repEnt.read_time = DateTime.UtcNow;
            else
                repEnt.read_time = updateRepEnt.read_time;
            repEnt.sho_reply = updateRepEnt.sho_reply;
            if (repEnt.sho_reply == null)
                repEnt.sho_reply_time = DateTime.UtcNow;
            else
                repEnt.sho_reply_time = updateRepEnt.sho_reply_time;

            repEnt.dcp_reply = updateRepEnt.dcp_reply;
            repEnt.dcp_reply_public = updateRepEnt.dcp_reply_public;
            if (repEnt.dcp_reply == null)
                repEnt.dcp_reply_time = DateTime.UtcNow;
            else
                repEnt.dcp_reply_time = updateRepEnt.dcp_reply_time;
            
            repEnt.sms_sho_phone = updateRepEnt.sms_sho_phone;
            if (repEnt.sms_send_time.HasValue)
                repEnt.sms_send_time = updateRepEnt.sms_send_time.Value;
            repEnt.sms_sent = updateRepEnt.sms_sent;

            repEnt.category = updateRepEnt.category;
            repEnt.imageid = updateRepEnt.imageid;

            //Assign new values
            repEnt.dcp_non_public_reply = dcp_non_public_reply;
            repEnt.dcp_non_public_reply_time = DateTime.UtcNow;

            //Delete old entry
            tableServiceContext.DeleteObject(updateRepEnt);
            tableServiceContext.SaveChangesWithRetries();

            //Add new Entity
            tableServiceContext.AddObject(tableName, repEnt);
            tableServiceContext.SaveChangesWithRetries();
        }


        //get District from PS ID
        public IEnumerable<DistrictEntity> getDistrictFromPsId(string ps_id)
        {
            //get District ID from ps_id
            PSObjectDataSource psDS = new PSObjectDataSource();
            var psEntObj = psDS.getAllPoliceStationsById(ps_id);

            int dist_id = 0;
            foreach (PSObjectEntity psEnt in psEntObj)
                dist_id = psEnt.districtid;
            
            //get District from dist_id
            DistrictDataSource distDS = new DistrictDataSource();
            var distEntObj = distDS.getADistrict(dist_id.ToString());

            return distEntObj;
        }


        //get Image Id from PK and RK
        public string getImageId(string pk, string rk)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            ReportsObjectEntity repEnt = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                                                where g.PartitionKey == pk
                                                && g.RowKey == rk
                                                select g).FirstOrDefault();
            return repEnt.imageid;
        }


        //get Feedback Photos
        public CloudBlobContainer getFeedPhotos()
        {
            storageAccount = CloudStorageAccount.Parse(RoleEnvironment.GetConfigurationSettingValue(connectionName));
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            //get Blob container
            CloudBlobContainer blobContainer = blobClient.GetContainerReference("feedbackphotos");

            return blobContainer;
        }


        //Delete Feedback
        public void delFeedback(string pk, string rk)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            ReportsObjectEntity repEnt = (from g in tableServiceContext.CreateQuery<ReportsObjectEntity>(tableName)
                                          where g.PartitionKey == pk
                                          && g.RowKey == rk
                                          select g).FirstOrDefault();

            tableServiceContext.DeleteObject(repEnt);
            tableServiceContext.SaveChangesWithRetries();
        }
    }
}
