﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace CitizenReports
{
    public class CrimeObjectDataSource
    {
        private string tableName = "Crimes";
        private string connectionName = "PSConn";
        private static CloudStorageAccount storageAccount;
        private CloudTableClient tableClient;

        public CrimeObjectDataSource()
        {
            storageAccount = CloudStorageAccount.Parse(RoleEnvironment.GetConfigurationSettingValue(connectionName));
            tableClient = storageAccount.CreateCloudTableClient();
        }


        //Put Crimes Data
        public void putCrimes(CrimeObjectEntity crimeEnt)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            tableClient.CreateTableIfNotExist(tableName);

            tableServiceContext.AddObject(tableName, crimeEnt);
            tableServiceContext.SaveChangesWithRetries();
        }

        //Get Crimes with offset and limit
        public IEnumerable<CrimeObjectEntity> getCrimes(int offset = 0, int limit = 10)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();
            tableClient.CreateTableIfNotExist(tableName);

            var repObjs = (from g in tableServiceContext.CreateQuery<CrimeObjectEntity>(tableName)
                           select g).AsEnumerable<CrimeObjectEntity>().Skip(offset).Take(limit);
            return repObjs;
        }

        //Get All Crimes
        public IEnumerable<CrimeObjectEntity> getAllCrimes()
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var repObjs = (from g in tableServiceContext.CreateQuery<CrimeObjectEntity>(tableName)
                           select g).AsEnumerable<CrimeObjectEntity>();
            return repObjs;
        }

        //Search
        public IEnumerable<CrimeObjectEntity> getSearchCrimes(string ps_id)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var repObjs = (from g in tableServiceContext.CreateQuery<CrimeObjectEntity>(tableName)
                           where g.PartitionKey == ps_id
                           select g).AsEnumerable<CrimeObjectEntity>();
            return repObjs;
        }

        //Get A Crime
        public CrimeObjectEntity getACrime(string partitionKey, string rowKey)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            CrimeObjectEntity crimeObj = (from g in tableServiceContext.CreateQuery<CrimeObjectEntity>(tableName) 
                                          where g.PartitionKey == partitionKey && g.RowKey == rowKey 
                                          select g).FirstOrDefault();
            return crimeObj;
        }


        //Update Crime
        public void updateCrime(CrimeObject crimeObj, string partitionKey, string rowKey)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            CrimeObjectEntity crimeEnt = new CrimeObjectEntity(crimeObj);

            CrimeObjectEntity updateCrimeEnt = (from g in tableServiceContext.CreateQuery<CrimeObjectEntity>(tableName) 
                                                    where g.PartitionKey == partitionKey && g.RowKey == rowKey 
                                                    select g).FirstOrDefault();

            crimeEnt.RowKey = updateCrimeEnt.RowKey;
            crimeEnt.entityid = updateCrimeEnt.entityid;
            crimeEnt.date = updateCrimeEnt.date;

            //Delete Current Entity
            tableServiceContext.DeleteObject(updateCrimeEnt);
            tableServiceContext.SaveChangesWithRetries();

            //Save as new entity
            tableServiceContext.AddObject(tableName, crimeEnt);
            tableServiceContext.SaveChangesWithRetries();
        }


        //Delete Crime
        public void deleteCrime(string partitionKey, string rowKey)
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            CrimeObjectEntity delCrimeObj = (from g in tableServiceContext.CreateQuery<CrimeObjectEntity>(tableName)
                                                where g.PartitionKey == partitionKey && g.RowKey == rowKey
                                                select g).FirstOrDefault();

            tableServiceContext.DeleteObject(delCrimeObj);
            tableServiceContext.SaveChangesWithRetries();
        }

        //Delete Crime Table
        public void deleteAllCrimes()
        {
            TableServiceContext tableServiceContext = tableClient.GetDataServiceContext();

            var delCrimeObjs = (from g in tableServiceContext.CreateQuery<CrimeObjectEntity>(tableName) 
                               select g).AsEnumerable<CrimeObjectEntity>();

             foreach(CrimeObjectEntity delCrimeObj in delCrimeObjs)
             {
                tableServiceContext.DeleteObject(delCrimeObj);
                tableServiceContext.SaveChangesWithRetries();
             }
        }

    }
}
