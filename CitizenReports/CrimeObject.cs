﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CitizenReports
{
    public class CrimeObject
    {
        public int ps_id;
        public int incident_count;
        public DateTime date;
        public int district_id;
        public string crime_head;
    }
}
