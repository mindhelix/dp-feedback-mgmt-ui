﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.WindowsAzure.StorageClient;

namespace CitizenReports
{
    public class ReportsObjectEntity : TableServiceEntity
    {
        public ReportsObjectEntity(ReportObject repObj)
        {
            this.PartitionKey = repObj.ps_id.ToString();
            this.RowKey = DateTime.UtcNow.Ticks.ToString() + "_" + Guid.NewGuid().ToString();
            this.name = repObj.name;
            this.email = repObj.email;
            this.phone = repObj.phone;
            this.details = repObj.details;
            this.policestation = repObj.policestation;
            this.date_time = repObj.date_time;
            this.latitude = repObj.latitude;
            this.longitude = repObj.longitude;
            //this.location_name = repObj.location_name;
            this.approval_stat = repObj.approval_stat;
            this.read_time = repObj.read_time;
            this.ipaddress = repObj.ipaddress;

            //new properties
            this.sho_reply = repObj.sho_reply;
            this.sho_reply_time = repObj.sho_reply_time;
            this.dcp_reply = repObj.dcp_reply;
            this.dcp_reply_public = repObj.dcp_reply_public;
            this.dcp_reply_time = repObj.dcp_reply_time;

            this.dcp_non_public_reply = repObj.dcp_non_public_reply;
            this.dcp_non_public_reply_time = repObj.dcp_non_public_reply_time;

            this.sms_send_time = repObj.sms_send_time;
            this.sms_sent = repObj.sms_sent;
            this.sms_sho_phone = repObj.sms_sho_phone;

            this.category = repObj.category;

            this.imageid = repObj.imageid;
        }

        public ReportsObjectEntity() { }

        public string policestation { get; set; }
        public DateTime date_time { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        //public string location_name { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string details { get; set; }
        public bool approval_stat { get; set; }
        public DateTime read_time { get; set; }
        public string ipaddress { get; set; }

        //new properties
        public string sho_reply { get; set; }
        public DateTime sho_reply_time { get; set; }
        public string dcp_reply { get; set; }
        public bool dcp_reply_public { get; set; }
        public DateTime dcp_reply_time { get; set; }

        public string dcp_non_public_reply { get; set; }
        public DateTime dcp_non_public_reply_time { get; set; }

        public DateTime? sms_send_time { get; set; }
        public bool sms_sent { get; set; }
        public string sms_sho_phone { get; set; }

        public string category { get; set; }

        public string imageid { get; set; }
    }
}
