﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CitizenReports
{
    public class ReportObject
    {
        public ReportObject()
        {
            //default values
            approval_stat = false;
            sho_reply = "";
            dcp_reply = "";
            dcp_reply_public = false;
        }

        public int ps_id;
        public DateTime date_time;
        public string policestation;
        public string name;
        public string email;
        public string phone;
        public string details;
        public double latitude;
        public double longitude;
        //public string location_name;
        public bool approval_stat;
        public DateTime read_time;
        public string ipaddress;

        //new properties
        public string sho_reply;
        public DateTime sho_reply_time;
        public string dcp_reply;
        public bool dcp_reply_public;
        public DateTime dcp_reply_time;

        public string dcp_non_public_reply;
        public DateTime dcp_non_public_reply_time;

        public DateTime? sms_send_time;
        public bool sms_sent;
        public string sms_sho_phone;

        public string category;

        public string imageid;
    }
}
