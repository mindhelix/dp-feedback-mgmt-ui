﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.WindowsAzure.StorageClient;

namespace CitizenReports
{
    public class UserObjectEntity : TableServiceEntity
    {
        public UserObjectEntity(UserObject user)
        {
            this.PartitionKey = DateTime.UtcNow.ToString("ddMMyyyy");
            this.RowKey = DateTime.UtcNow.Ticks.ToString() + "_" + Guid.NewGuid().ToString();
            this.Username = user.username;
            this.Password = user.password;
            this.Level = user.level;
            this.Status = user.status;
        }

        public UserObjectEntity() { }

        public string Username { get; set; }
        public string Password { get; set; }
        public int Level { get; set; }
        public int Status { get; set; }
     }
}
