﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CitizenReports
{
    public class PSObject
    {
        public int id;
        public string stationname;
        public int districtid;
        public string shoid;
        public string shophone;
        public string shomobile;
        public string shoemail;
        public double latitude;
        public double longitude;
        public string kmlsnippet;
        public string address;
    }
}
