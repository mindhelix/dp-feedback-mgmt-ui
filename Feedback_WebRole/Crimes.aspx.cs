﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using CitizenReports;

namespace Feedback_WebRole
{
    public partial class Crimes : System.Web.UI.Page
    {
        int offset = 0;
        int limit = 10;
        int total_page = 1;
        int page_count = 1;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["logged_in"] == null)
                Response.Redirect("Login.aspx", true);
            if (Session["level"].ToString() != "admin")
                Response.Redirect("Logout.aspx", true);

            if (!IsPostBack)
            {
                ViewState["offset"] = offset.ToString();
                ViewState["total_page"] = total_page.ToString();
                ViewState["page_count"] = page_count.ToString();

                //Initialize Grid
                fillGrid();
            }
        }

        //Initialize PoliceStations Grid
        public void fillGrid()
        {
            CrimeObjectDataSource crimeDS = new CrimeObjectDataSource();
            var crimeObj = crimeDS.getCrimes();

            if (crimeObj.Count() == 0)
            {
                grid_crimes.DataSource = null;
                grid_crimes.DataBind();
            }
            else
            {
                grid_crimes.DataSource = crimeObj;
                grid_crimes.DataBind();
            }

            //Replace PS Id with Police Station
            replace_PSid_To_PSName();


            //Paging ops
            int item_count = crimeDS.getAllCrimes().Count<CrimeObjectEntity>();
            total_page = item_count / 10;
            if ((item_count % 10) != 0)
                total_page++;

            lbl_page_stat.Text = "Page " + page_count + " of " + total_page;

            if (page_count < total_page)
                btn_next.Enabled = true;


            ViewState["total_page"] = total_page.ToString();
        }


        //Replace PS Id with Police Station name
        public void replace_PSid_To_PSName()
        {
            bool flag = false;

            for (int i = 0; i < grid_crimes.Rows.Count; i++)
            {
                string ps_id = grid_crimes.Rows[i].Cells[3].Text;
                PSObjectDataSource psDS = new PSObjectDataSource();
                var psEnt = psDS.getAllPoliceStationsById(ps_id);

                flag = true;

                foreach (PSObjectEntity ps in psEnt)
                {
                    flag = false;
                    grid_crimes.Rows[i].Cells[3].Text = String.Format("{1} ({0})", grid_crimes.Rows[i].Cells[3].Text, ps.stationname);
                }

                if (flag == true)
                    grid_crimes.Rows[i].Cells[3].Text = String.Format("N/A ({0})", grid_crimes.Rows[i].Cells[3].Text);
            }
        }

        //Paging Next btn
        protected void btn_next_Click(object sender, EventArgs e)
        {
            lbl_upload_crime_resp.Text = "Update Crime Details from CSV file:";
            btn_prev.Enabled = true;

            offset = Int32.Parse(ViewState["offset"].ToString());
            offset += 10;

            CrimeObjectDataSource crimeDS = new CrimeObjectDataSource();

            grid_crimes.DataSource = crimeDS.getCrimes(offset, limit);
            grid_crimes.DataBind();

            //Replace PS Id with Police Station
            replace_PSid_To_PSName();

            page_count = Int32.Parse(ViewState["page_count"].ToString());
            total_page = Int32.Parse(ViewState["total_page"].ToString());

            lbl_page_stat.Text = "Page " + ++page_count + " of " + total_page;

            ViewState["offset"] = offset.ToString();
            ViewState["page_count"] = page_count.ToString();

            if (page_count == Int32.Parse(ViewState["total_page"].ToString()))
                btn_next.Enabled = false;
        }


        //Paging Prev btn
        protected void btn_prev_Click(object sender, EventArgs e)
        {
            if (btn_next.Enabled == false)
                btn_next.Enabled = true;

            offset = Int32.Parse(ViewState["offset"].ToString());
            offset -= 10;

            CrimeObjectDataSource crimeDS = new CrimeObjectDataSource();
            
            grid_crimes.DataSource = crimeDS.getCrimes(offset, limit);
            grid_crimes.DataBind();

            //Replace PS Id with Police Station
            replace_PSid_To_PSName();

            page_count = Int32.Parse(ViewState["page_count"].ToString());
            total_page = Int32.Parse(ViewState["total_page"].ToString());

            lbl_page_stat.Text = "Page " + --page_count + " of " + total_page;

            ViewState["offset"] = offset.ToString();
            ViewState["page_count"] = page_count.ToString();

            if (page_count == 1)
                btn_prev.Enabled = false;
        }

        
        //Edit Crime Details
        protected void grid_crimes_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["Crime_PK"] = grid_crimes.DataKeys[grid_crimes.SelectedIndex].Values[0].ToString();
            Session["Crime_RK"] = grid_crimes.DataKeys[grid_crimes.SelectedIndex].Values[1].ToString();

            Response.Redirect("EditCrime.aspx", true);
        }

        //Delete Crime Details
        protected void btn_del_crime_Click(object sender, EventArgs e)
        {
            CrimeObjectDataSource crimeDS = new CrimeObjectDataSource();

            for (int i = 0; i < grid_crimes.Rows.Count; i++)
            {
                CheckBox chk_del = new CheckBox();
                chk_del = (CheckBox)grid_crimes.Rows[i].Cells[0].FindControl("chk_del_crime");

                if (chk_del.Checked == true)
                {
                    string partitionKey = grid_crimes.DataKeys[i].Values[0].ToString();
                    string rowKey = grid_crimes.DataKeys[i].Values[1].ToString();

                    //del entry
                    crimeDS.deleteCrime(partitionKey, rowKey);
                    grid_crimes.Rows[i].Visible = false;
                }
            }
        }

        protected void btn_revert_Cr_Click(object sender, EventArgs e)
        {
            Response.Redirect("Crimes.aspx", true);
        }



        protected void btn_go_Cr_Click(object sender, EventArgs e)
        {
            string ps_id = txt_search_Cr.Text;

            searchCr(ps_id);
        }


        //Search
        public void searchCr(string ps_id)
        {
            btn_next.Enabled = false;
            btn_prev.Enabled = false;
            lbl_page_stat.Text = "Page 1 of 1";


            CrimeObjectDataSource crimeDS = new CrimeObjectDataSource();

            grid_crimes.DataSource = crimeDS.getSearchCrimes(ps_id);
            grid_crimes.DataBind();

            //Replace PS Id with Police Station
            replace_PSid_To_PSName(); 
        }


        
        //Update Crime Data from CSV file
        protected void btn_update_crime_csv_Click(object sender, EventArgs e)
        {
            lbl_upload_crime_resp.Text = "Uploading data. Please wait...";
            btn_update_crime_csv.Enabled = false;

            if (file_crime_csv.HasFile)
            {
                string extension = Path.GetExtension(file_crime_csv.FileName);

                if (extension != ".csv" && extension != ".CSV")
                {
                    lbl_upload_crime_resp.Text = "Invalid file type. Please upload a valid csv file.";
                    btn_update_crime_csv.Enabled = true;
                    return;
                }

                //read CSV
                read_CSV_File(file_crime_csv.FileContent);


                lbl_upload_crime_resp.Text = "Crime data updated successfully.";
                btn_update_crime_csv.Enabled = true;    
            }
            
        }
        //End of Update Crime data from CSV


        //Read CSV file
        public void read_CSV_File(Stream fileContent)
        {
            StreamReader readFile = new StreamReader(fileContent);

            string line;
            string[] row;
            bool firstRowFlag = true;
            int lineCount = 0;

            //check number of items in each row
            while ((line = readFile.ReadLine()) != null)
            {
                ++lineCount;

                //check first row
                if (firstRowFlag == true)
                {
                    firstRowFlag = false;

                    if (line != "PS_Id,Incident_Count,Date,District_Id,Crime_Head")
                    {
                        lbl_upload_crime_resp.Text = "Invalid Feedback csv format! Error at Line No." + lineCount;
                        return;
                    }
                }

                row = line.Split(',');
                if (row.Length != 5)
                {
                    lbl_upload_crime_resp.Text = "Invalid Feedback csv format! Error at Line No." + lineCount;
                    return;
                }
            }

            //Everything seems fine till now,
            //so delete the table and replace with new CSV data provided

            //Delete Table
            CrimeObjectDataSource crimeDS = new CrimeObjectDataSource();
            crimeDS.deleteAllCrimes();

            lineCount = 0;
            readFile.BaseStream.Position = 0;
            firstRowFlag = true;
            while ((line = readFile.ReadLine()) != null)
            {
                ++lineCount;

                if (firstRowFlag)
                {
                    firstRowFlag = false;
                    continue;
                }

                row = line.Split(',');

                CrimeObject crimeObj = new CrimeObject();
                crimeObj.ps_id = Int32.Parse(row[0]);
                crimeObj.incident_count = Int32.Parse(row[1]);
                crimeObj.date = DateTime.Now.Date;
                crimeObj.district_id = Int32.Parse(row[3]);
                crimeObj.crime_head = row[4];

                CrimeObjectEntity crimeEnt = new CrimeObjectEntity(crimeObj);
                crimeDS.putCrimes(crimeEnt);

            }

            //ReFill the grid with new data
            offset = 0;
            total_page = 1;
            page_count = 1;
            ViewState["offset"] = offset.ToString();
            ViewState["total_page"] = total_page.ToString();
            ViewState["page_count"] = page_count.ToString();

            fillGrid(); 
        }
        //End of read_CSV
    }
}