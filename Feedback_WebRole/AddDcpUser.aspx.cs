﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Security.Cryptography;
using CitizenReports;

namespace Feedback_WebRole
{
    public partial class AddDcpUser : System.Web.UI.Page
    {
        UserObjectDataSource userDS = new UserObjectDataSource();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["logged_in"] == null)
                Response.Redirect("Login.aspx", true);
        }


        //Back to User Control
        protected void btn_add_dcp_back_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserControl.aspx");
        }


        //Add DCP User submit
        protected void btn_add_dcp_submit_Click(object sender, EventArgs e)
        {
            //check if user already exists
            if (userDS.doesUserExists(txt_dcp_user.Text))
            {
                lbl_add_dcp_rsp.Text = "The username already exists! Try a different username.";
                return;
            }

            UserObject userObj = new UserObject();
            userObj.username = txt_dcp_user.Text;
            
            //Compute MD5 of password
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5 = new MD5CryptoServiceProvider();

            originalBytes = ASCIIEncoding.Default.GetBytes(txt_dcp_pass.Text);
            encodedBytes = md5.ComputeHash(originalBytes);

            userObj.password = BitConverter.ToString(encodedBytes);


            userObj.level = 2; //DCP level
            userObj.status = 1; //Active


            //create user
            UserObjectEntity newUserEnt = userDS.CreateUser(userObj);


            //add user data
            //add DCP's district
            UserDataObject userData = new UserDataObject();
            userData.user_id = newUserEnt.RowKey;
            userData.data_key = "District_Id";
            userData.data_value = drop_add_dcp_district.SelectedItem.Value;

            //add to storage
            UserDataDataSource userDataDS = new UserDataDataSource();
            userDataDS.AddUserData(userData);

            txt_dcp_user.Text = "";
            txt_dcp_pass.Text = "";
            lbl_add_dcp_rsp.Text = "DCP user created.";
        }

        //check user exists
        protected void txt_dcp_user_TextChanged(object sender, EventArgs e)
        {
            //check if user already exists
            if (userDS.doesUserExists(txt_dcp_user.Text))
            {
                lbl_add_dcp_rsp.Text = "The username already exists! Try a different username.";
                txt_dcp_user.Focus();
            }
            else
            {
                lbl_add_dcp_rsp.Text = "The username available.";
                txt_dcp_pass.Focus();
            }
        }
    }
}