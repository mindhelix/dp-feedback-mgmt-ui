﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DcpNPReply.aspx.cs" Inherits="Feedback_WebRole.DcpNPReply" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>DCP's Non Public Reply</h2>

    <table class="style1">
        <tr>
            <td>
                <div class="feedback_item">
                    <p>
                        <asp:Label ID="Label1" runat="server" Text="Police Station: "></asp:Label>
                        <asp:Label ID="lbl_ps_name_feeds" runat="server" Text=""></asp:Label>
                    </p>
                    <p>
                        <asp:Label ID="Label2" runat="server" Text="Citizen Name: "></asp:Label>
                        <asp:Label ID="lbl_citizen_name_feeds" runat="server" Text=""></asp:Label>
                    </p>
                    <p>
                        <asp:Label ID="Label3" runat="server" Text="Email: "></asp:Label>
                        <asp:Label ID="lbl_email_feeds" runat="server" Text=""></asp:Label>
                    </p>
                    <p>
                        <asp:Label ID="Label4" runat="server" Text="Feedback: "></asp:Label></p>
 
                    <div class="feed_content">
                        <asp:Literal ID="lit_feed_content" runat="server" Text=""></asp:Literal>
                    </div>
                    <div class="feed_details">
                        <img src="Images/date-time.png" />
                        Date Time: <asp:Literal ID="lit_feed_timestamp" runat="server" Text=""></asp:Literal> &nbsp;
                        <img src="Images/ip.png" />
                        IP Address: <asp:Literal ID="lit_feed_ip" runat="server" Text=""></asp:Literal> &nbsp;
                        <img src="Images/location-globe.png" />
                        Location: <asp:Literal ID="lit_feed_lat" runat="server" Text=""></asp:Literal>, 
                        <asp:Literal ID="lit_feed_long" runat="server" Text=""></asp:Literal>
                    </div>
                    <div style="clear:both"></div>
                            
                    <div class="feed_stat">
                    </div>
                    <div class="feed_del">
                        <%--<asp:LinkButton ID="lnk_del_feed" runat="server" CommandName="Delete" Text="Delete" />--%>
                                
                        <%--I'm going to put all hidden values here--%>
                        <asp:HiddenField ID="hidden_feed_pk" runat="server" Value="" />
                        <asp:HiddenField ID="hidden_feed_rk" runat="server" Value="" /> 
                    </div>
                    <div style="clear:both"></div>

                    <asp:Panel ID="panel_sho_reply" runat="server" Visible="false">
                        <p>
                            <asp:Label ID="Label7" runat="server" Text="SHO's Reply: "></asp:Label></p>
                        <div class="feed_content">
                            <asp:Literal ID="lit_sho_reply" runat="server" Text=""></asp:Literal>
                        </div>
                        <div class="feed_details">
                            <asp:Label ID="Label9" runat="server" Text="Reply Time: "></asp:Label>
                            <asp:Label ID="lbl_sho_reply_time" runat="server" Text=""></asp:Label>
                        </div>
                        <div style="clear:both"></div>
                    </asp:Panel>

                    <p>
                        <asp:Label ID="Label5" runat="server" Text="DCP's Non Public Reply: "></asp:Label></p>

                    <asp:TextBox ID="txt_dcp_reply" runat="server" 
                        Height="75px" TextMode="MultiLine" Width="98%"></asp:TextBox>

                    <p>
                        <asp:Button ID="btn_dcp_reply" runat="server" Text="Submit" 
                            onclick="btn_dcp_reply_Click" />
                        <asp:Label ID="lbl_dcp_reply_submit_rsp" runat="server" Text=""></asp:Label>
                    </p>

                    <p>
                        <asp:Button ID="btn_back_from_dcp_reply" runat="server" Text="&lt;&lt; Back" 
                            onclick="btn_back_from_dcp_reply_Click" /></p>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
