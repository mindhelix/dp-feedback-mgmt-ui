﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PoliceStations.aspx.cs" Inherits="Feedback_WebRole.PoliceStations" MaintainScrollPositionOnPostback="true" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: autp;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>
        Police Station Details
    </h2>
    
    
        <table class="style1">
            <tr>
                <td align="right">
        <asp:TextBox ID="txt_search_PS" runat="server" 
            ontextchanged="txt_search_PS_TextChanged"></asp:TextBox>
                    <asp:TextBoxWatermarkExtender ID="txt_search_PS_TextBoxWatermarkExtender" 
                        runat="server" Enabled="True" TargetControlID="txt_search_PS" 
                        WatermarkText="Search PS ID">
                    </asp:TextBoxWatermarkExtender>
&nbsp;<asp:Button ID="btn_search_PS" runat="server" onclick="btn_search_ps_Click" 
            Text="Go" />
&nbsp; <asp:Button ID="btn_revert_viewPS" runat="server" 
            onclick="btn_revert_viewPS_Click" Text="Revert View" />
                </td>
            </tr>
            <tr>
                <td>
        <asp:GridView ID="grid_ps" runat="server" AutoGenerateColumns="False" 
            CellPadding="4" ForeColor="#333333" GridLines="None" 
            onselectedindexchanged="grid_ps_SelectedIndexChanged" ShowFooter="True" 
                        DataKeyNames="PartitionKey,RowKey">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:TemplateField>
                    <FooterTemplate>
                        <asp:Button ID="btn_del_ps" runat="server" onclick="btn_del_ps_Click" 
                            Text="Delete" />
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chk_del_ps" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="PartitionKey" HeaderText="PartitionKey" 
                    Visible="False" />
                <asp:BoundField DataField="RowKey" HeaderText="RowKey" Visible="False" />
                <asp:BoundField DataField="id" HeaderText="PS ID" />
                <asp:BoundField DataField="stationname" HeaderText="Police Station" />
                <asp:BoundField DataField="districtid" HeaderText="District ID" />
                <asp:BoundField DataField="shoid" HeaderText="SHO ID" />
                <asp:BoundField DataField="shophone" HeaderText="SHO Phone" />
                <asp:BoundField DataField="shomobile" HeaderText="SHO Mobile" />
                <asp:BoundField DataField="shoemail" HeaderText="SHO Email" />
                <asp:BoundField DataField="address" HeaderText="Address" />
                <asp:CommandField SelectText="Edit" ShowSelectButton="True" />
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>

        <asp:Button ID="btn_prev" runat="server" Enabled="False" 
            onclick="btn_prev_Click" Text="&lt;&lt; Prev" />
&nbsp;<asp:Label ID="lbl_page_stat" runat="server"></asp:Label>
&nbsp;<asp:Button ID="btn_next" runat="server" Enabled="False" onclick="btn_next_Click" 
            Text="Next &gt;&gt;" />

                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    <p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;</p>
    </asp:Content>
