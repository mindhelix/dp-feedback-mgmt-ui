﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CitizenReports;

namespace Feedback_WebRole
{
    public partial class EditPoliceStation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["logged_in"] == null)
                Response.Redirect("Login.aspx", true);

            if (!IsPostBack)
            {
                //Fill SHO ID drop down
                fillShoIdDrop();

                //Fill Edit fields
                fillEditPoliceStation(Session["PS_PK"].ToString(), Session["PS_RK"].ToString());
            }
        }


        //Fill SHO ID drop down
        public void fillShoIdDrop()
        {
            OfficerObjectDataSource officerDS = new OfficerObjectDataSource();

            var officerEnts = officerDS.getAllOfficers();

            drop_sho_ids.DataSource = officerEnts;
            drop_sho_ids.DataTextField = "id";
            drop_sho_ids.DataValueField = "id";
            drop_sho_ids.DataBind();
        }

        //Fill Edit fields
        public void fillEditPoliceStation(string partitionKey, string rowKey)
        {
            PSObjectDataSource psDS = new PSObjectDataSource();

            PSObjectEntity psObj = new PSObjectEntity();
            psObj = psDS.getAPoliceStation(partitionKey, rowKey);

            txt_station.Text = psObj.stationname;

            for (int i = 0; i < drop_sho_ids.Items.Count; i++)
            {
                if (drop_sho_ids.Items[i].Value == psObj.shoid)
                {
                    drop_sho_ids.SelectedIndex = i;
                    break;
                }
            }

            txt_shophone.Text = psObj.shophone;
            txt_shomobile.Text = psObj.shomobile;
            txt_shoemail.Text = psObj.shoemail;
            txt_lat.Text = psObj.latitude.ToString();
            txt_long.Text = psObj.longitude.ToString();
            txt_address.Text = psObj.address;
        }


        //Update Police Station
        protected void btn_updatePS_Click(object sender, EventArgs e)
        {
            btn_updatePS.Enabled = false;
            lbl_ps_response.Text = "Please wait...";

            PSObject psObj = new PSObject();
            
            psObj.stationname = txt_station.Text;
            psObj.shoid = drop_sho_ids.SelectedItem.Value;
            psObj.shophone = txt_shophone.Text;
            psObj.shomobile = txt_shomobile.Text;
            psObj.shoemail = txt_shoemail.Text;
            psObj.latitude = Double.Parse(txt_lat.Text);
            psObj.longitude = Double.Parse(txt_long.Text);
            psObj.address = txt_address.Text;

            PSObjectDataSource psDS = new PSObjectDataSource();
            psDS.updatePoliceStation(psObj, Session["PS_PK"].ToString(), Session["PS_RK"].ToString());

            lbl_ps_response.Text = "Updated successfully.";
            btn_updatePS.Enabled = true;
        }

        protected void btn_backToPS_Click(object sender, EventArgs e)
        {
            Response.Redirect("PoliceStations.aspx", true);
        }
    }
}