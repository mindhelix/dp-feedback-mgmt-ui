﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddSho.aspx.cs" Inherits="Feedback_WebRole.AddSho" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 395px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Add New SHO</h2>

    
    <table class="style1">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="SHO ID :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_add_sho_id" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="SHO Name :"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_add_sho_name" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="SHO Photo :"></asp:Label>
            </td>
            <td>
                <asp:FileUpload ID="file_add_sho_photo" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_add_sho_pic_rsp" runat="server" 
                    Text="The image must be in .jpg format"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_add_new_sho" runat="server" onclick="btn_add_new_sho_Click" 
                    Text="Save" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btn_back2sho" runat="server" onclick="btn_back2sho_Click" 
                    Text="&lt;&lt; Back" />
            </td>
            <td>
                <asp:Label ID="lbl_add_sho_rsp" runat="server"></asp:Label>
            </td>
        </tr>
    </table>

    
</asp:Content>
