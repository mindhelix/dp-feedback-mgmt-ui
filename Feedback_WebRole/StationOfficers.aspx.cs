﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CitizenReports;
using Microsoft.WindowsAzure.StorageClient;

namespace Feedback_WebRole
{
    public partial class StationOfficers : System.Web.UI.Page
    {
        OfficerObjectDataSource officerDS = new OfficerObjectDataSource();

        int offset = 0;
        int limit = 10;
        int total_page = 1;
        int page_count = 1;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["logged_in"] == null)
                Response.Redirect("Login.aspx", true);
            if (Session["level"].ToString() != "admin" && Session["level"].ToString() != "phq")
                Response.Redirect("Logout.aspx", true);

            if (Session["level"].ToString() == "phq")
            {
                //Disable Nav
                Menu NavigationMenu = new Menu();
                NavigationMenu = (Menu)Master.FindControl("NavigationMenu");
                NavigationMenu.Visible = false;

                //Enable PHQ Menu
                Menu NavMenuPhq = new Menu();
                NavMenuPhq = (Menu)Master.FindControl("NavMenuPhq");
                NavMenuPhq.Visible = true;
            }

            if (!IsPostBack)
            {
                //Paging setup
                ViewState["offset"] = offset.ToString();
                ViewState["total_page"] = total_page.ToString();
                ViewState["page_count"] = page_count.ToString();

                //Fill Sho Listview
                fill_shoList();
            }
        }

        //Fill Sho ListView
        public void fill_shoList(int skip = 0, int take = 10)
        {
            //Fill ListView
            list_officers.DataSource = officerDS.getOfficers(skip, take);
            list_officers.DataBind();

            //Fill Sho Photos
            fill_sho_photos();

            //Paging ops
            int item_count = officerDS.getAllOfficers().Count<OfficerObjectEntity>();
            total_page = item_count / 10;
            if ((item_count % 10) != 0)
                total_page++;

            lbl_page_stat.Text = "Page " + page_count + " of " + total_page;

            if (page_count < total_page)
                btn_next.Enabled = true;


            ViewState["total_page"] = total_page.ToString();
        }

        //Fill Sho Photos
        public void fill_sho_photos()
        {
            CloudBlobContainer blobContainer = officerDS.getShoPhotos();

            for (int i = 0; i < list_officers.Items.Count; i++)
            {
                Label lbl_shoid = new Label();
                lbl_shoid = (Label)list_officers.Items[i].FindControl("lbl_sho_id");

                CloudBlob blob = blobContainer.GetBlobReference(string.Format("{0}.jpg", lbl_shoid.Text));


                Image img = new Image();
                img = (Image)list_officers.Items[i].FindControl("img_sho_photo");

                try
                {
                    blob.FetchAttributes();
                    img.ImageUrl = blob.Uri.ToString();
                }
                catch (StorageClientException sce)
                {
                    img.ImageUrl = "Images/empty-profile.png";
                }

            }


            
        }


        //ListView Selected Index Changed
        protected void list_officers_SelectedIndexChanging(object sender, ListViewSelectEventArgs e)
        {
            //get Index
            list_officers.SelectedIndex = e.NewSelectedIndex;
            var i = list_officers.SelectedIndex;

            //get SHO ID
            Label lbl_shoid = new Label();
            lbl_shoid = (Label)list_officers.Items[i].FindControl("lbl_sho_id");
            var shoid = lbl_shoid.Text;

            //get PartitionKey
            HiddenField hidden_officer_pk = new HiddenField();
            hidden_officer_pk = (HiddenField)list_officers.Items[i].FindControl("hidden_officer_pk");
            var officer_pk = hidden_officer_pk.Value;

            Session["SHO_ID"] = shoid;
            Session["OFFICER_PK"] = officer_pk;

            Response.Redirect("EditOfficer.aspx", true);
        }


        //Delete Officer
        protected void list_officers_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            //selected index
            var i = e.ItemIndex;

            //get SHO ID
            Label lbl_shoid = new Label();
            lbl_shoid = (Label)list_officers.Items[i].FindControl("lbl_sho_id");
            var shoid = lbl_shoid.Text;

            //get PartitionKey
            HiddenField hidden_officer_pk = new HiddenField();
            hidden_officer_pk = (HiddenField)list_officers.Items[i].FindControl("hidden_officer_pk");
            var officer_pk = hidden_officer_pk.Value;


            //delete
            officerDS.deleteSho(shoid, officer_pk);
            list_officers.Items[i].Visible = false;
        }

        //Paging Next Btn
        protected void btn_next_Click(object sender, EventArgs e)
        {
            btn_prev.Enabled = true;

            offset = Int32.Parse(ViewState["offset"].ToString());
            offset += 10;

            fill_shoList(offset);

            page_count = Int32.Parse(ViewState["page_count"].ToString());
            total_page = Int32.Parse(ViewState["total_page"].ToString());

            lbl_page_stat.Text = "Page " + ++page_count + " of " + total_page;

            ViewState["offset"] = offset.ToString();
            ViewState["page_count"] = page_count.ToString();

            if (page_count == Int32.Parse(ViewState["total_page"].ToString()))
                btn_next.Enabled = false;
        }


        //Paging Prev Btn
        protected void btn_prev_Click(object sender, EventArgs e)
        {
            if (btn_next.Enabled == false)
                btn_next.Enabled = true;

            offset = Int32.Parse(ViewState["offset"].ToString());
            offset -= 10;

            fill_shoList(offset);

            page_count = Int32.Parse(ViewState["page_count"].ToString());
            total_page = Int32.Parse(ViewState["total_page"].ToString());

            lbl_page_stat.Text = "Page " + --page_count + " of " + total_page;

            ViewState["offset"] = offset.ToString();
            ViewState["page_count"] = page_count.ToString();

            if (page_count == 1)
                btn_prev.Enabled = false;
        }


        //Revert
        protected void btn_sho_revert_view_Click(object sender, EventArgs e)
        {
            panel_paging.Visible = true;
            Response.Redirect("StationOfficers.aspx", true);
        }


        //Search SHO ID
        protected void btn_shoid_search_Click(object sender, EventArgs e)
        {
            string sho_id = txt_search_shoid.Text;
            if (sho_id == "")
                return;

            searchShoID(sho_id);
        }


        //search 
        public void searchShoID(string sho_id)
        {
            panel_paging.Visible = false;

            //Fill ListView
            list_officers.DataSource = officerDS.getOfficerById(sho_id);
            list_officers.DataBind();

            //Fill Sho Photos
            fill_sho_photos();
        }



        //Add SHO
        protected void btn_add_sho_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddSho.aspx");
        }

    }
}