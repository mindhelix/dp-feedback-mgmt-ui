﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="Feedback_WebRole._Default" MaintainScrollPositionOnPostback="true" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .style1
        {
            width: auto;
        }
    </style>

    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC53Gyndlj4pk7h4OisLOQ3oPQGneH_k_4&sensor=false"></script>
    <script type="text/javascript">

        var map;
        var latLng_list = "<%=latLng_List%>";
        var marker;
        var markerArr = [];

        function initialize() {
            var NewDelhi = new google.maps.LatLng(28.635157, 77.229767);
            var myOptions = {
                zoom: 10,
                center: NewDelhi,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            /*marker = new google.maps.Marker({
                position: NewDelhi,
                title: "New Delhi"
            });

            marker.setMap(map);*/

            if (latLng_list != "") {
                var latLngArr = latLng_list.split("|");
                for (var i = 0; i < latLngArr.length; i++) {
                    var latLng = latLngArr[i].split(",");
                    var lat = latLng[0];
                    var lng = latLng[1];
                    addMarker(lat, lng);
                }
                showMarkers();
            } else {
                deleteMarkers();
            }
        }

        
        //add Marker
        function addMarker(lat, lng) {
            var latLng = new google.maps.LatLng(lat, lng);
            marker = new google.maps.Marker({
                    position: latLng
                });
            
            markerArr.push(marker);
        }

        //show Markers
        function showMarkers() {
            if (markerArr) {
                for (i in markerArr) {
                    markerArr[i].setMap(map);
                }
            }
        }

        //delete Markers
        function deleteMarkers() {
            if (markerArr) {
                for (i in markerArr) {
                    markerArr[i].setMap(null);
                }
                markerArr.length = 0;
            }
        }
        
        //load Map
        google.maps.event.addDomListener(window, 'load', initialize);    
    </script>

</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Citizen Feedbacks
    </h2>
    
        <%--<asp:Button ID="btn_post" runat="server" onclick="btn_post_Click" 
            Text="Post Data" />--%>
    
        <table class="style1" width="auto">
            <tr>
                <td align="right" colspan="2">
                    <asp:Label ID="Label1" runat="server" Text="Search: "></asp:Label>
                    <asp:DropDownList ID="drop_policestations" runat="server" 
                        AppendDataBoundItems="True">
                    </asp:DropDownList>
                    <asp:ListSearchExtender ID="drop_policestations_ListSearchExtender" 
                        runat="server" Enabled="True" IsSorted="True" QueryPattern="Contains" 
                        TargetControlID="drop_policestations">
                    </asp:ListSearchExtender>
        &nbsp;<asp:DropDownList ID="drop_open_noted" runat="server">
            <asp:ListItem Value="0">Open/Noted</asp:ListItem>
            <asp:ListItem Value="1">Open</asp:ListItem>
            <asp:ListItem Value="2">Noted</asp:ListItem>
        </asp:DropDownList>
&nbsp;<asp:Button ID="btn_search_feedback" runat="server" 
            onclick="btn_search_feedback_Click" Text="Go" />
&nbsp; <asp:Button ID="btn_revert_view" runat="server" onclick="btn_revert_view_Click" 
            Text="Revert View" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Panel ID="panel_map" runat="server">
                        <div id="map_canvas" style="width:911px; height:300px"></div>
                    </asp:Panel>
                    <asp:CollapsiblePanelExtender ID="panel_map_CollapsiblePanelExtender" 
                        runat="server" CollapseControlID="btn_revert_view" Collapsed="True" 
                        Enabled="True" ExpandControlID="btn_search_feedback" 
                        TargetControlID="panel_map">
                    </asp:CollapsiblePanelExtender>
                    <asp:RoundedCornersExtender ID="panel_map_RoundedCornersExtender" 
                        runat="server" BorderColor="182, 183, 188" Enabled="True" 
                        TargetControlID="panel_map">
                    </asp:RoundedCornersExtender>
                </td>
            </tr>
            <tr>
                <td colspan="2">
        <asp:GridView ID="grid_reports" runat="server" AutoGenerateColumns="False" 
            BorderStyle="None" BorderWidth="1px" CellPadding="2" 
            ForeColor="#333333" GridLines="None" BorderColor="Black" 
            ShowHeaderWhenEmpty="True" CellSpacing="1" DataKeyNames="PartitionKey,RowKey" 
                        Width="914px">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="PartitionKey" HeaderText="PS ID" Visible="False" />
                <asp:BoundField DataField="policestation" HeaderText="Police Station" />
                <asp:BoundField DataField="RowKey" HeaderText="RowKey" Visible="False" />
                <asp:BoundField DataField="name" HeaderText="Citizen Name" />
                <asp:BoundField DataField="email" HeaderText="Email" />
                <asp:BoundField DataField="details" HeaderText="Feedback" >
                <ItemStyle Width="175px" />
                </asp:BoundField>
                <asp:BoundField DataField="ipaddress" HeaderText="IP Address" />
                <asp:BoundField DataField="latitude" HeaderText="Latitude" />
                <asp:BoundField DataField="longitude" HeaderText="Longitude" />
                <asp:BoundField DataField="approval_stat" HeaderText="Open/Noted" />
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
   
        <asp:Button ID="btn_prev" runat="server" Enabled="False" 
            onclick="btn_prev_Click" Text="&lt;&lt; Prev" />
                    <asp:Label ID="lbl_page_stat" runat="server" Text="Page 1 of 1"></asp:Label>
                    <asp:Button ID="btn_next" runat="server" Enabled="False" onclick="btn_next_Click" 
            Text="Next &gt;&gt;" />
                </td>
            </tr>
            <tr>
                <td>
        <asp:Button ID="btn_approve" runat="server" onclick="btn_approve_Click" 
            Text="Mark Noted" />
                    <asp:Button ID="btn_disapprove" runat="server" onclick="btn_disapprove_Click" 
            Text="Disapprove" Enabled="False" Visible="False" />
                </td>
                <td align="right">
        <asp:Button ID="btn_export_feedback" runat="server" 
            onclick="btn_export_feedback_Click" Text="Export as CSV" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>

        <p>
            &nbsp;</p>
</asp:Content>
