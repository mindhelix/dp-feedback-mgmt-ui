﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CitizenFeedbacks.aspx.cs" Inherits="Feedback_WebRole.CitizenFeedbacks" MaintainScrollPositionOnPostback="true" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>

    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAH_j30-Rv9knLG6DjjF_xZv9pBWhTO9Zk&sensor=false"></script>
    <script type="text/javascript">

        var map;
        var latLng_list = "<%=latLng_List%>";
        var marker;
        var markerArr = [];

        function initialize() {
            var NewDelhi = new google.maps.LatLng(28.635157, 77.229767);
            var myOptions = {
                zoom: 9,
                center: NewDelhi,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            /*marker = new google.maps.Marker({
            position: NewDelhi,
            title: "New Delhi"
            });

            marker.setMap(map);*/

            if (latLng_list != "") {
                var latLngArr = latLng_list.split("|");
                for (var i = 0; i < latLngArr.length; i++) {
                    var latLng = latLngArr[i].split(",");
                    var lat = latLng[0];
                    var lng = latLng[1];
                    addMarker(lat, lng);
                }
                showMarkers();
            } else {
                deleteMarkers();
            }
        }


        //add Marker
        function addMarker(lat, lng) {
            var latLng = new google.maps.LatLng(lat, lng);
            marker = new google.maps.Marker({
                position: latLng
            });

            markerArr.push(marker);
        }

        //show Markers
        function showMarkers() {
            if (markerArr) {
                for (i in markerArr) {
                    markerArr[i].setMap(map);
                }
            }
        }

        //delete Markers
        function deleteMarkers() {
            if (markerArr) {
                for (i in markerArr) {
                    markerArr[i].setMap(null);
                }
                markerArr.length = 0;
            }
        }

        //load Map
        google.maps.event.addDomListener(window, 'load', initialize);    
    </script>

    <script type="text/javascript">
        function CancelFeedDel() {
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>&nbsp;Citizen Feedbacks</h2>
    

    <table class="style1">
        <tr>
            <td align="right">
                <asp:Label ID="Label6" runat="server" Text="Search:"></asp:Label>
&nbsp;<asp:DropDownList ID="drop_feedback_search" runat="server">
                </asp:DropDownList>
                <asp:ListSearchExtender ID="drop_feedback_search_ListSearchExtender" 
                    runat="server" Enabled="True" QueryPattern="Contains" 
                    TargetControlID="drop_feedback_search">
                </asp:ListSearchExtender>
                <asp:DropDownList ID="drop_feedback_search_options" runat="server">
                    <asp:ListItem Value="0">Read/Unread</asp:ListItem>
                    <asp:ListItem Value="1">Unread</asp:ListItem>
                    <asp:ListItem Value="2">Read</asp:ListItem>
                </asp:DropDownList>
                <asp:Button ID="btn_feedback_search_go" runat="server" Text="Go" 
                    onclick="btn_feedback_search_go_Click" />
&nbsp;<asp:Button ID="btn_export_feeds" runat="server" Text="Export as .csv" 
                    onclick="btn_export_feeds_Click" />
&nbsp;<asp:Button ID="btn_feedback_revert_view" runat="server" Text="Revert View" 
                    onclick="btn_feedback_revert_view_Click" />
            </td>
        </tr>
        <tr>
            <td>
                <div id="map_canvas" style="width:99%; height:300px"></div>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="panel_list_feeds" runat="server">
                    
                
                <asp:ListView ID="list_feedbacks_view" runat="server" 
                        onselectedindexchanging="list_feedbacks_view_SelectedIndexChanging" 
                        onitemediting="list_feedbacks_view_ItemEditing" 
                        onitemcommand="list_feedbacks_view_ItemCommand" 
                        onitemdeleting="list_feedbacks_view_ItemDeleting">

                    <LayoutTemplate>
                        
                            <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                        
                    </LayoutTemplate>

                    <ItemTemplate>
                        <div class="feedback_item">
                            <p>
                                <asp:Label ID="Label1" runat="server" Text="Police Station: "></asp:Label>
                                <asp:Label ID="lbl_ps_name_feeds" runat="server" Text='<%#Eval("policestation") %>'></asp:Label> &nbsp;

                                <asp:Label ID="Label14" runat="server" Text="District: "></asp:Label>
                                <asp:Label ID="lbl_ps_district" runat="server" Text=""></asp:Label> &nbsp;

                                <asp:Label ID="Label15" runat="server" Text="Range: "></asp:Label>
                                <asp:Label ID="lbl_dist_range" runat="server" Text=""></asp:Label>
                            </p>
                            <p>
                                <asp:Label ID="Label2" runat="server" Text="Citizen Name: "></asp:Label>
                                <asp:Label ID="lbl_citizen_name_feeds" runat="server" Text='<%#Eval("name") %>'></asp:Label>
                            </p>
                            <p>
                                <asp:Label ID="Label3" runat="server" Text="Email: "></asp:Label>
                                <asp:Label ID="lbl_email_feeds" runat="server" Text='<%#Eval("email") %>'></asp:Label>
                            </p>
                            <p>
                                <asp:Label ID="Label17" runat="server" Text="Report Category: "></asp:Label>
                                <asp:Label ID="lbl_report_cat" runat="server" Text='<%#Eval("category") %>'></asp:Label>
                            </p>
                            <p>
                                <asp:Label ID="Label4" runat="server" Text="Feedback: "></asp:Label></p>
                            <div class="feed_content">
                                <p>
                                    <asp:Literal ID="lbl_feed_content" runat="server" Text='<%#Eval("details") %>'></asp:Literal>
                                </p>
                                <asp:Image ID="img_feed_image" CssClass="feed_image" runat="server" ImageUrl="Images/noImageAvailable.gif" />
                                <div class="clear"></div>
                            </div>
                            <div class="feed_details">
                                <img src="Images/date-time.png" />
                                <asp:Label ID="lbl_feed_time_label" runat="server" Text="Date Time:"></asp:Label>
                                <asp:Literal ID="lit_feed_timestamp" runat="server" Text='<%#Eval("date_time") %>'></asp:Literal> &nbsp;
                            <%
                                if (Session["level"].ToString() == "admin")
                                {%>
                                    <img src="Images/ip.png" />
                                    <asp:Label ID="lbl_ip_addr_label" runat="server" Text="IP Address:"></asp:Label>
                                    <asp:Literal ID="lit_feed_ip" runat="server" Text='<%#Eval("ipaddress") %>'></asp:Literal> &nbsp;
                                    <img src="Images/location-globe.png" />
                                    <asp:Label ID="lbl_loc_label" runat="server" Text="Location:"></asp:Label>
                                    <asp:Literal ID="lit_feed_lat" runat="server" Text='<%#Eval("latitude") %>'></asp:Literal>
                                    <asp:Literal ID="lit_feed_lat_long_comma" runat="server" Text=", "></asp:Literal>
                                    <asp:Literal ID="lit_feed_long" runat="server" Text='<%#Eval("longitude") %>'></asp:Literal>    
                                <%}
                             %>
                                
                            </div>
                            <div style="clear:both"></div>
                            
                            <div class="feed_stat">
                                <asp:Label ID="Label5" runat="server" Text="Mark as "></asp:Label> 
                                <asp:LinkButton ID="lnk_mark_read_feed" runat="server" CommandName="Select" Text='<%#Eval("approval_stat") %>' />.
                                &nbsp;<asp:Label ID="lbl_read_time_label" runat="server" Text="Read Time: " Visible="false"></asp:Label>
                                <asp:Label ID="lbl_read_time" runat="server" Text='<%#Eval("read_time") %>' Visible="false"></asp:Label>
                            </div>
                            <div class="feed_del">
                                <% if (Session["level"].ToString() == "admin")
                                   { %>
                                    <asp:LinkButton ID="lnk_del_feed" runat="server" CommandName="Delete" Text="Delete" ForeColor="Red" />
                                    <asp:ConfirmButtonExtender ID="lnk_del_feed_ConfirmButtonExtender" 
                                        runat="server" ConfirmText="Are you sure you want to delete?" OnClientCancel="CancelFeedDel" Enabled="True" TargetControlID="lnk_del_feed">
                                    </asp:ConfirmButtonExtender>
                                <% } %>

                                <%--I'm going to put all hidden values here--%>
                                <asp:HiddenField ID="hidden_feed_pk" runat="server" Value='<%#Eval("PartitionKey") %>' />
                                <asp:HiddenField ID="hidden_feed_rk" runat="server" Value='<%#Eval("RowKey") %>' />
                                
                            </div>
                            <div style="clear:both"></div>
                       

                            <!-- SHO Reply -->
                            <asp:Panel ID="panel_sho_reply" runat="server" CssClass="reply_panels">
                                <p>
                                <asp:Label ID="Label7" runat="server" Text="SHO Comment: "></asp:Label></p>
                                <div class="feed_content">
                                    <asp:Literal ID="lit_sho_reply" runat="server" Text='<%#Eval("sho_reply") %>'></asp:Literal>
                                </div>
                                <div class="feed_details">
                                    <asp:Label ID="Label9" runat="server" Text="Comment Time: "></asp:Label>
                                    <asp:Label ID="Label10" runat="server" Text='<%#Eval("sho_reply_time") %>'></asp:Label>
                                </div>
                                <div style="clear:both"></div>    
                            </asp:Panel>
                            
                            <!-- DCP Reply -->
                            <asp:Panel ID="panel_dcp_reply" runat="server" CssClass="reply_panels">
                                <p>
                                <asp:Label ID="Label8" runat="server" Text="DCP Comment: "></asp:Label></p>
                                <div class="feed_content">
                                    <asp:Literal ID="lit_dcp_reply" runat="server" Text='<%#Eval("dcp_reply") %>'></asp:Literal>
                                </div>
                                <div class="feed_details">
                                    <asp:Label ID="Label11" runat="server" Text="Comment to General Public: "></asp:Label>
                                    <asp:Label ID="lbl_dcp_reply_public" runat="server" Text='<%#Eval("dcp_reply_public") %>'></asp:Label>&nbsp;

                                    <asp:Label ID="Label12" runat="server" Text="Comment Time: "></asp:Label>
                                    <asp:Label ID="Label13" runat="server" Text='<%#Eval("dcp_reply_time") %>'></asp:Label>
                                </div>
                                <div style="clear:both"></div>    
                            </asp:Panel>


                            <!-- DCP Non Public Reply -->
                            <asp:Panel ID="panel_dcp_non_public_reply" runat="server" CssClass="reply_panels">
                                <p>
                                <asp:Label ID="Label16" runat="server" Text="DCP Non Public Comment: "></asp:Label></p>
                                <div class="feed_content">
                                    <asp:Literal ID="lit_dcp_non_public_reply" runat="server" Text='<%#Eval("dcp_non_public_reply") %>'></asp:Literal>
                                </div>
                                <div class="feed_details">
                                    <asp:Label ID="Label19" runat="server" Text="Comment Time: "></asp:Label>
                                    <asp:Label ID="Label20" runat="server" Text='<%#Eval("dcp_non_public_reply_time") %>'></asp:Label>
                                </div>
                                <div style="clear:both"></div>    
                            </asp:Panel>

                            <div class="feed_stat" style="margin-top:10px; font-size:12px;">
                                <asp:LinkButton ID="lnk_add_dcp_reply" runat="server" CommandName="Edit" Text="DCP's Comment" /> &nbsp;
                                <asp:LinkButton ID="lnk_add_dcp_non_public_reply" runat="server" CommandName="Add" Text="DCP's Non Public Comment" />
                            </div>
                            <div class="clear"></div>
                        </div>
                    </ItemTemplate>

                    <EditItemTemplate>
                        <div>&nbsp;</div>
                    </EditItemTemplate>

                    <EmptyDataTemplate>
                        <div>
                            No feedbacks available.
                        </div>
                    </EmptyDataTemplate>
                </asp:ListView>


                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="btn_feeds_prev" runat="server" Enabled="False" 
                    Text="&lt;&lt; Prev" onclick="btn_feeds_prev_Click" />
&nbsp;<asp:Label ID="lbl_feeds_page_stat" runat="server" Text="Page 1 of 1"></asp:Label>
&nbsp;<asp:Button ID="btn_feeds_next" runat="server" Text="Next &gt;&gt;" 
                    onclick="btn_feeds_next_Click" Enabled="False" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    

</asp:Content>
