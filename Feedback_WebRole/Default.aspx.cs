﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CitizenReports;

namespace Feedback_WebRole
{
    public partial class _Default : System.Web.UI.Page
    {

        int offset = 0;
        int limit = 10;
        int total_page = 1;
        int page_count = 1;

        public string latLng_List = "";

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["logged_in"] == null)
                Response.Redirect("Login.aspx", true);

            if (!IsPostBack)
            {
                ViewState["offset"] = offset.ToString();
                ViewState["total_page"] = total_page.ToString();
                ViewState["page_count"] = page_count.ToString(); 
  
                //Initialize Grid
                fillGrid();

                //Dropdown
                fillPSDrop();
            }
        }


        //Initialize PS Dropdown
        public void fillPSDrop()
        {
            ReportsObjectDataSource repDS = new ReportsObjectDataSource();
            var repObj = repDS.getAllPoliceStations();

            drop_policestations.DataSource = repObj.OrderBy(x=>x.policestation);
            drop_policestations.DataTextField = "policestation";
            drop_policestations.DataBind();
            drop_policestations.Items.Insert(0, new ListItem(""));
        }

        //Initialize Reports Grid
        public void fillGrid()
        {
            ReportsObjectDataSource repDS = new ReportsObjectDataSource();
            var repObj = repDS.getFeedbacks();

            grid_reports.DataSource = repObj;
            grid_reports.DataBind();
            //grid_reports.Columns[1].Visible = false;
            //grid_reports.Columns[2].Visible = false;

            set_approval_stat(); //Now noted stat

            //Paging ops
            int item_count = repDS.getAllFeedbacks().Count<ReportsObjectEntity>();
            total_page = item_count / 10;
            if ((item_count % 10) != 0)
                total_page++;

            lbl_page_stat.Text = "Page " + page_count + " of " + total_page;

            if (page_count < total_page)
                btn_next.Enabled = true;


            ViewState["total_page"] = total_page.ToString();
        }

        //Approval Stat correction // Now Noted stat
        public void set_approval_stat()
        {
            for (int i = 0; i < grid_reports.Rows.Count; i++)
            {
                if (grid_reports.Rows[i].Cells[10].Text == "False")
                    grid_reports.Rows[i].Cells[10].Text = "Open";
                else
                    grid_reports.Rows[i].Cells[10].Text = "Noted";
            }
        }

        //Paging Next btn
        protected void btn_next_Click(object sender, EventArgs e)
        {
            btn_prev.Enabled = true;

            offset = Int32.Parse(ViewState["offset"].ToString());
            offset += 10;

            ReportsObjectDataSource repDS = new ReportsObjectDataSource();

            //grid_reports.Columns[1].Visible = true;
            grid_reports.Columns[2].Visible = true;
            grid_reports.DataSource = repDS.getFeedbacks(offset, limit);
            grid_reports.DataBind();
            //grid_reports.Columns[1].Visible = false;
            //grid_reports.Columns[2].Visible = false;

            set_approval_stat();

            page_count = Int32.Parse(ViewState["page_count"].ToString());
            total_page = Int32.Parse(ViewState["total_page"].ToString());

            lbl_page_stat.Text = "Page " + ++page_count + " of " + total_page;

            ViewState["offset"] = offset.ToString();
            ViewState["page_count"] = page_count.ToString();

            if (page_count == Int32.Parse(ViewState["total_page"].ToString()))
                btn_next.Enabled = false;
        }


        //Paging Prev btn
        protected void btn_prev_Click(object sender, EventArgs e)
        {
            if (btn_next.Enabled == false)
                btn_next.Enabled = true;

            offset = Int32.Parse(ViewState["offset"].ToString());
            offset -= 10;

            ReportsObjectDataSource repDS = new ReportsObjectDataSource();

            //grid_reports.Columns[1].Visible = true;
            grid_reports.Columns[2].Visible = true;
            grid_reports.DataSource = repDS.getFeedbacks(offset, limit);
            grid_reports.DataBind();
            //grid_reports.Columns[1].Visible = false;
            //grid_reports.Columns[2].Visible = false;

            set_approval_stat();

            page_count = Int32.Parse(ViewState["page_count"].ToString());
            total_page = Int32.Parse(ViewState["total_page"].ToString());

            lbl_page_stat.Text = "Page " + --page_count + " of " + total_page;

            ViewState["offset"] = offset.ToString();
            ViewState["page_count"] = page_count.ToString();

            if (page_count == 1)
                btn_prev.Enabled = false;
        }


        //Test Data
        protected void btn_post_Click(object sender, EventArgs e)
        {
            ReportObject rep = new ReportObject();

            rep.ps_id = 1238;
            rep.name = "Tiger";
            rep.email = "tiger@scott.com";
            rep.details = "blah blah blah";

            ReportsObjectDataSource repDS = new ReportsObjectDataSource();
            repDS.postFeedback(rep);
        }


        //Feedback approval //Now Mark as Noted
        protected void btn_approve_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < grid_reports.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)grid_reports.Rows[i].Cells[0].FindControl("CheckBox1");
                if (chk.Checked == true)
                {
                    grid_reports.Rows[i].Cells[10].Text = "Noted";
                    if (drop_open_noted.SelectedIndex == 1)
                        grid_reports.Rows[i].Visible = false;

                    ReportsObjectDataSource repDS = new ReportsObjectDataSource();
                    

                    //repDS.updateApproval(grid_reports.Rows[i].Cells[1].Text, grid_reports.Rows[i].Cells[2].Text, true);
                    repDS.updateApproval(grid_reports.DataKeys[i].Values[0].ToString(), grid_reports.DataKeys[i].Values[1].ToString(), true); 
                }
            }
        }

        //Feedback disapproval
        protected void btn_disapprove_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < grid_reports.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)grid_reports.Rows[i].Cells[0].FindControl("CheckBox1");
                if (chk.Checked == true)
                {
                    grid_reports.Rows[i].Cells[10].Text = "Open";
                    ReportsObjectDataSource repDS = new ReportsObjectDataSource();

                    repDS.updateApproval(grid_reports.DataKeys[i].Values[0].ToString(), grid_reports.DataKeys[i].Values[1].ToString(), false); 
                }
            }
        }


        
        //Export Feedback as CSV
        protected void btn_export_feedback_Click(object sender, EventArgs e)
        {
            //Http Context
            HttpContext context = HttpContext.Current;
            context.Response.Clear();

            //Set Content type and header
            context.Response.ContentType = "text/csv";
            context.Response.AddHeader("Content-Disposition", "attachment; filename=Citizen_Feedbacks.csv");

            //Write Header Columns
            context.Response.Write("PS Id, PoliceStation, Citizen Name, Phone, Email, Comments, Latitude, Longitude, IP Address, Date Time, Read/Unread");
            context.Response.Write(Environment.NewLine);

            //Get Feedbacks and add rows
            ReportsObjectDataSource repDS = new ReportsObjectDataSource();
            var repEnts = ((drop_policestations.SelectedItem.Text == "") && (drop_open_noted.SelectedIndex == 0)) ? repDS.getAllFeedbacks() : repDS.getSearchedFeedbacks(drop_policestations.SelectedItem.Text, drop_open_noted.SelectedItem.Value);
            

            //Add CSV rows
            foreach(ReportsObjectEntity repEnt in repEnts)
            {
                //PS Id
                context.Response.Write(repEnt.PartitionKey + ",");
                //PoliceStation
                context.Response.Write("\"" + repEnt.policestation + "\",");
                //Citizen Name
                context.Response.Write("\"" + repEnt.name + "\",");
                //Phone
                context.Response.Write("\"" + repEnt.phone + "\",");
                //Email
                context.Response.Write("\"" + repEnt.email + "\",");
                //Comments
                context.Response.Write("\"" + repEnt.details + "\",");
                //Latitude
                context.Response.Write(repEnt.latitude + ",");
                //Longitude
                context.Response.Write(repEnt.longitude + ",");
                //IP Address
                context.Response.Write(repEnt.ipaddress + ",");
                //Date Time
                context.Response.Write("\"" + repEnt.date_time + "\",");
                //Open/Noted
                if(repEnt.approval_stat == false)
                    context.Response.Write("\"Open\"");
                else
                    context.Response.Write("\"Noted\"");

                //NewLine
                context.Response.Write(Environment.NewLine);
            }

            //End of file
            context.Response.End();
        }

        
        //Search
        protected void btn_search_feedback_Click(object sender, EventArgs e)
        {
            string ps_name = drop_policestations.SelectedItem.Text;
            string search_type = drop_open_noted.SelectedItem.Value;

            //search
            searchResult(ps_name, search_type);
        }



        //Display Grid based on keywords
        public void searchResult(string ps_name, string search_type = "0")
        {
            btn_next.Enabled = false;
            btn_prev.Enabled = false;
            lbl_page_stat.Text = "Page 1 of 1";

            ReportsObjectDataSource repDS = new ReportsObjectDataSource();
            
            //grid_reports.Columns[2].Visible = true;
            grid_reports.DataSource = repDS.getSearchedFeedbacks(ps_name, search_type);
            grid_reports.DataBind();

            set_approval_stat();
            //grid_reports.Columns[2].Visible = false;
            
            setLatLngs();
        }


        //reset view
        protected void btn_revert_view_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx", true);
        }

        
        //set latLng_list
        public void setLatLngs()
        {
            for (int i = 0; i < grid_reports.Rows.Count; i++)
            {
                latLng_List += grid_reports.Rows[i].Cells[8].Text + "," + grid_reports.Rows[i].Cells[9].Text;
                if (i != grid_reports.Rows.Count - 1)
                    latLng_List += "|";
            }
        }
    }
}
