﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddPhqUser.aspx.cs" Inherits="Feedback_WebRole.AddPhqUser" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
    .style1
    {
        width: 450px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Add PHQ User</h2>

    <table class="style1">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Username:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_add_phq_user" runat="server" 
                    ontextchanged="txt_add_phq_user_TextChanged"></asp:TextBox>
&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txt_add_phq_user" ErrorMessage="This field is mandatory" 
                    ForeColor="White" ValidationGroup="phq_user">*</asp:RequiredFieldValidator>
                <asp:ValidatorCalloutExtender ID="RequiredFieldValidator1_ValidatorCalloutExtender" 
                    runat="server" Enabled="True" TargetControlID="RequiredFieldValidator1">
                </asp:ValidatorCalloutExtender>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Password:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_add_phq_pass" runat="server"></asp:TextBox>
&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="txt_add_phq_pass" ErrorMessage="This field is mandatory" 
                    ForeColor="White" ValidationGroup="phq_user">*</asp:RequiredFieldValidator>
                <asp:ValidatorCalloutExtender ID="RequiredFieldValidator2_ValidatorCalloutExtender" 
                    runat="server" Enabled="True" TargetControlID="RequiredFieldValidator2">
                </asp:ValidatorCalloutExtender>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_phq_create" runat="server" onclick="btn_phq_create_Click" 
                    Text="Save" ValidationGroup="phq_user" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_add_phq_user_rsp" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btn_back2UCfromPHQ" runat="server" 
                    onclick="btn_back2UCfromPHQ_Click" Text="&lt;&lt; Back" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
</table>
</asp:Content>
