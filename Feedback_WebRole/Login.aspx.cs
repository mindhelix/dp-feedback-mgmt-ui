﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Security.Cryptography;
using CitizenReports;

namespace Feedback_WebRole
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txt_username.Focus();
        }

        protected void btn_login_Click(object sender, EventArgs e)
        {
            lbl_response.Text = "Please wait...";

            UserObjectDataSource userDS = new UserObjectDataSource();

            //Passwd to md5
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5 = new MD5CryptoServiceProvider();

            originalBytes = ASCIIEncoding.Default.GetBytes(txt_password.Text);
            encodedBytes = md5.ComputeHash(originalBytes);

            string md5_passwd = BitConverter.ToString(encodedBytes);

            var userObj = userDS.checkUser(txt_username.Text, md5_passwd);

            if (userObj != null)
            {
                if (userObj.Status != 1)
                {
                    lbl_response.Text = "Inactive user. Contact your administrator for more details.";
                    return;
                }

                Session["logged_in"] = txt_username.Text;
                Session["user_id"] = userObj.RowKey;

                if (userObj.Level == 1)
                    Session["level"] = "admin";
                else if (userObj.Level == 2)
                {
                    Session["level"] = "dcp";
                    //get DCP's District
                    UserDataDataSource userDataDS = new UserDataDataSource();
                    UserDataEntity userData = userDataDS.GetUserData(userObj.RowKey, "District_Id");
                    Session["District_Id"] = userData.data_value;
                }
                else if (userObj.Level == 3)
                {
                    Session["level"] = "range";
                    //get Range User's Range
                    UserDataDataSource userDataDS = new UserDataDataSource();
                    UserDataEntity userData = userDataDS.GetUserData(userObj.RowKey, "Range");
                    Session["Range"] = userData.data_value;
                }
                else if (userObj.Level == 4)
                {
                    Session["level"] = "phq";
                    Response.Redirect("PoliceStations.aspx", true);
                }
                else if (userObj.Level == 10) //Temp Read-only user
                {
                    Session["level"] = "read-only";
                    Response.Redirect("ReadOnlyFeedbacks.aspx", true);
                }

                
                Response.Redirect("CitizenFeedbacks.aspx", true);
                //Response.Redirect("Default.aspx", true);
            }
            else
                lbl_response.Text = "Invalid Username or Password!";
        }
    }
}