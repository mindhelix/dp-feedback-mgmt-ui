﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditOfficer.aspx.cs" Inherits="Feedback_WebRole.EditOfficer" MaintainScrollPositionOnPostback="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 563px;
        }
        .style2
        {
            width: 61px;
        }
        .style3
        {
            width: 83px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Edit Officer</h2>

    <table class="style1">
        <tr>
            <td rowspan="5" class="style3">
                <asp:Image ID="img_officer_photo" runat="server" Height="150px" Width="150px" />
            </td>
            <td class="style2">
                <asp:Label ID="Label1" runat="server" Text="SHO ID:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_sho_id" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                <asp:Label ID="Label2" runat="server" Text="SHO Name"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_sho_name" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">&nbsp;</td>
            <td>
                <asp:Button ID="btn_update_officer" runat="server" Text="Update" 
                    onclick="btn_update_officer_Click" />
            &nbsp;<asp:Label ID="lbl_officer_update_rsp" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:FileUpload ID="fileup_sho_photo" runat="server" />
&nbsp;<asp:Button ID="btn_upload_sho_photo" runat="server" Text="Upload Photo" 
                    onclick="btn_upload_sho_photo_Click" />
                <asp:LinkButton ID="lnk_del_sho_pic" runat="server" 
                    onclick="lnk_del_sho_pic_Click">Remove Photo</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="lbl_sho_pic_rsp" runat="server" ForeColor="#666666" 
                    Text="The image must be in .jpg format"></asp:Label>
            </td>
        </tr>
    </table>
    <p>
        <asp:Button ID="btn_backFromShoEdit" runat="server" 
            onclick="btn_backFromShoEdit_Click" Text="&lt;&lt; Back" />
    </p>
</asp:Content>
