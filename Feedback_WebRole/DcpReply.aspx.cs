﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CitizenReports;

namespace Feedback_WebRole
{
    public partial class DcpReply : System.Web.UI.Page
    {
        ReportsObjectDataSource repDS = new ReportsObjectDataSource();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["logged_in"] == null)
                Response.Redirect("Login.aspx", true);

            if (!IsPostBack)
            {
                var repEntEnum = repDS.getAFeedback(Session["Reports_PK"].ToString(), Session["Reports_RK"].ToString());

                foreach (ReportsObjectEntity repEnt in repEntEnum)
                {
                    lbl_ps_name_feeds.Text = repEnt.policestation;
                    lbl_citizen_name_feeds.Text = repEnt.name;
                    lbl_email_feeds.Text = repEnt.email;
                    lit_feed_content.Text = repEnt.details;
                    lit_feed_timestamp.Text = repEnt.date_time.ToString();
                    lit_feed_ip.Text = repEnt.ipaddress;
                    lit_feed_lat.Text = repEnt.latitude.ToString();
                    lit_feed_long.Text = repEnt.longitude.ToString();

                    //If sho reply exists
                    if (repEnt.sho_reply != "" && repEnt.sho_reply != null)
                    {
                        panel_sho_reply.Visible = true;
                        lit_sho_reply.Text = repEnt.sho_reply;
                        lbl_sho_reply_time.Text = repEnt.sho_reply_time.ToString();
                    }

                    //If DCP reply exists
                    if (repEnt.dcp_reply != "" && repEnt.dcp_reply != null)
                    {
                        txt_dcp_reply.Text = repEnt.dcp_reply;
                        if (repEnt.dcp_reply_public == true)
                            chk_dcp_reply_public.Checked = true;
                    }

                    hidden_feed_pk.Value = Session["Reports_PK"].ToString();
                    hidden_feed_rk.Value = Session["Reports_RK"].ToString();
                }
            }
        }


        //DCP's reply submit
        protected void btn_dcp_reply_Click(object sender, EventArgs e)
        {
            bool reply_public = chk_dcp_reply_public.Checked ? true : false;
            repDS.updateDcpReply(Session["Reports_PK"].ToString(), Session["Reports_RK"].ToString(), txt_dcp_reply.Text, reply_public);

            lbl_dcp_reply_submit_rsp.Text = "Reply submitted successfully.";
        }

        protected void btn_back_from_dcp_reply_Click(object sender, EventArgs e)
        {
            Response.Redirect("CitizenFeedbacks.aspx", true);
        }
    }
}