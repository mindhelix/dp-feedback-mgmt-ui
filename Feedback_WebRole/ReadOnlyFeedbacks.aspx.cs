﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using CitizenReports;
using Microsoft.WindowsAzure.StorageClient;

namespace Feedback_WebRole
{
    public partial class ReadOnlyFeedbacks : System.Web.UI.Page
    {
        int offset = 0;
        int limit = 10;
        int total_page = 1;
        int page_count = 1;
        bool search_mode = false;
        public string latLng_List = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["logged_in"] == null)
                Response.Redirect("Login.aspx", true);

            //Disable Nav
            Menu NavigationMenu = new Menu();
            NavigationMenu = (Menu)Master.FindControl("NavigationMenu");
            NavigationMenu.Visible = false;

            if (!IsPostBack)
            {
                ViewState["offset"] = offset.ToString();
                ViewState["total_page"] = total_page.ToString();
                ViewState["page_count"] = page_count.ToString();
                ViewState["search_mode"] = search_mode;

                //Initialize Grid
                fillFeedsList();

                //Fill Search PS Drop
                fillPSDrop();
            }
        }


        //Initialize Feedbacks Listview
        public void fillFeedsList(int offset = 0, int limit = 10)
        {
            ReportsObjectDataSource repDS = new ReportsObjectDataSource();

            search_mode = bool.Parse(ViewState["search_mode"].ToString());

            if (!search_mode)
                list_feedbacks_view.DataSource = repDS.getFeedbacks(offset, limit);
            else
                list_feedbacks_view.DataSource = repDS.getFilteredFeedbacks(ViewState["ps_name"].ToString(), ViewState["search_type"].ToString(), offset, limit);
             
            
            list_feedbacks_view.DataBind();

            //set Map LatLng
            setLatLngs();

            //Set Read / Unread
            set_read_stat();

            //Set SHO/DCP replies
            set_replies();

            //Set Dists and Ranges
            setDistsRanges();

            //Set Feed Images
            setFeedImages();

            //Paging ops
            int item_count = search_mode ? repDS.getAllFilteredFeedbacks(ViewState["ps_name"].ToString(), ViewState["search_type"].ToString()).Count() : repDS.getAllFeedbacks().Count();
            total_page = item_count / 10;
            if ((item_count % 10) != 0)
                total_page++;

            page_count = Int32.Parse(ViewState["page_count"].ToString());
            lbl_feeds_page_stat.Text = "Page " + page_count + " of " + total_page;

            if (page_count < total_page)
                btn_feeds_next.Enabled = true;


            ViewState["total_page"] = total_page.ToString();
        }

        //Set Read / Unread
        public void set_read_stat()
        {
            for (int i = 0; i < list_feedbacks_view.Items.Count; i++)
            {
                LinkButton lnk_mark_read_feed = new LinkButton();
                lnk_mark_read_feed = (LinkButton)list_feedbacks_view.Items[i].FindControl("lnk_mark_read_feed");

                if (lnk_mark_read_feed.Text == "False")
                {
                    lnk_mark_read_feed.Text = "Unread"; //Edited for ReadOnly
                    lnk_mark_read_feed.ForeColor = Color.Red;
                }
                else
                {
                    lnk_mark_read_feed.Text = "Read"; //Edited for ReadOnly
                    lnk_mark_read_feed.ForeColor = Color.Green;

                    Label lbl_read_time_label = new Label();
                    lbl_read_time_label = (Label)list_feedbacks_view.Items[i].FindControl("lbl_read_time_label");
                    lbl_read_time_label.Visible = true;

                    Label lbl_read_time = new Label();
                    lbl_read_time = (Label)list_feedbacks_view.Items[i].FindControl("lbl_read_time");
                    lbl_read_time.Visible = true;
                }
            }
        }


        //Set Replies
        public void set_replies()
        {
            for (int i = 0; i < list_feedbacks_view.Items.Count; i++)
            {
                //Set SHO reply

                //sho reply literal
                Literal lit_sho_reply = new Literal();
                lit_sho_reply = (Literal)list_feedbacks_view.Items[i].FindControl("lit_sho_reply");

                //sho reply panel
                Panel panel_sho_reply = new Panel();
                panel_sho_reply = (Panel)list_feedbacks_view.Items[i].FindControl("panel_sho_reply");

                if (lit_sho_reply.Text == "")
                    panel_sho_reply.Visible = false;
                else
                    panel_sho_reply.Visible = true;

                //Set DCP reply

                //dcp reply literal
                Literal lit_dcp_reply = new Literal();
                lit_dcp_reply = (Literal)list_feedbacks_view.Items[i].FindControl("lit_dcp_reply");

                //dcp reply panel
                Panel panel_dcp_reply = new Panel();
                panel_dcp_reply = (Panel)list_feedbacks_view.Items[i].FindControl("panel_dcp_reply");

                if (lit_dcp_reply.Text == "")
                    panel_dcp_reply.Visible = false;
                else
                {
                    //dcp reply public - Yes/No
                    Label lbl_dcp_reply_public = new Label();
                    lbl_dcp_reply_public = (Label)list_feedbacks_view.Items[i].FindControl("lbl_dcp_reply_public");

                    if (lbl_dcp_reply_public.Text == "True")
                        lbl_dcp_reply_public.Text = "Yes";
                    else
                        lbl_dcp_reply_public.Text = "No";

                    panel_dcp_reply.Visible = true;

                    LinkButton lnk_add_dcp_reply = new LinkButton();
                    lnk_add_dcp_reply = (LinkButton)list_feedbacks_view.Items[i].FindControl("lnk_add_dcp_reply");
                    lnk_add_dcp_reply.Text = "Edit DCP's Comment";
                }
            }
        }


        //Set Dists and Ranges
        public void setDistsRanges()
        {
            ReportsObjectDataSource repDS = new ReportsObjectDataSource();

            for (int i = 0; i < list_feedbacks_view.Items.Count; i++)
            {
                //get hidden PK
                HiddenField hidden_feed_pk = new HiddenField();
                hidden_feed_pk = (HiddenField)list_feedbacks_view.Items[i].FindControl("hidden_feed_pk");

                //get District for ps_id
                var distEntObj = repDS.getDistrictFromPsId(hidden_feed_pk.Value);

                foreach (DistrictEntity distEnt in distEntObj)
                {
                    //get District Label
                    Label lbl_ps_district = new Label();
                    lbl_ps_district = (Label)list_feedbacks_view.Items[i].FindControl("lbl_ps_district");
                    lbl_ps_district.Text = distEnt.name;

                    //get Range Label
                    Label lbl_dist_range = new Label();
                    lbl_dist_range = (Label)list_feedbacks_view.Items[i].FindControl("lbl_dist_range");
                    lbl_dist_range.Text = distEnt.districtrange;
                }
            }
        }


        //Get Feeds Images
        protected void setFeedImages()
        {
            ReportsObjectDataSource repDS = new ReportsObjectDataSource();
            CloudBlobContainer blobContainer = repDS.getFeedPhotos();

            for (int i = 0; i < list_feedbacks_view.Items.Count; i++)
            {

                //get hidden PK
                HiddenField hidden_feed_pk = new HiddenField();
                hidden_feed_pk = (HiddenField)list_feedbacks_view.Items[i].FindControl("hidden_feed_pk");

                //get hidden RK
                HiddenField hidden_feed_rk = new HiddenField();
                hidden_feed_rk = (HiddenField)list_feedbacks_view.Items[i].FindControl("hidden_feed_rk");

                //get Image Id
                string imageid = repDS.getImageId(hidden_feed_pk.Value, hidden_feed_rk.Value);


                CloudBlob blob = null;
                if (imageid == null)
                    continue;
                else
                {
                    //get Image field
                    System.Web.UI.WebControls.Image img_feed_image = new System.Web.UI.WebControls.Image();
                    img_feed_image = (System.Web.UI.WebControls.Image)list_feedbacks_view.Items[i].FindControl("img_feed_image");

                    var imageRef = imageid.Split('/');
                    blob = blobContainer.GetBlobReference(imageRef[1]);
                    try
                    {
                        blob.FetchAttributes();
                        img_feed_image.ImageUrl = blob.Uri.ToString();
                    }
                    catch (StorageClientException sce)
                    {
                        img_feed_image.ImageUrl = "Images/noImageAvailable.gif";
                    }
                }
            }
        }


        //Initialize PS Dropdown
        public void fillPSDrop()
        {
            ReportsObjectDataSource repDS = new ReportsObjectDataSource();
            var repObj = repDS.getAllPoliceStations();

            drop_feedback_search.DataSource = repObj.OrderBy(x => x.policestation);
            drop_feedback_search.DataTextField = "policestation";
            drop_feedback_search.DataBind();
            drop_feedback_search.Items.Insert(0, new ListItem(""));
        }


        //Feeds Next Page
        protected void btn_feeds_next_Click(object sender, EventArgs e)
        {
            btn_feeds_prev.Enabled = true;

            offset = Int32.Parse(ViewState["offset"].ToString());
            ViewState["offset"] = (offset += 10).ToString();

            page_count = Int32.Parse(ViewState["page_count"].ToString());
            ViewState["page_count"] = (++page_count).ToString();

            
            fillFeedsList(offset);

            if (page_count == Int32.Parse(ViewState["total_page"].ToString()))
                btn_feeds_next.Enabled = false;

            drop_feedback_search.Focus();
        }


        //Feeds Prev Page
        protected void btn_feeds_prev_Click(object sender, EventArgs e)
        {
            if (btn_feeds_next.Enabled == false)
                btn_feeds_next.Enabled = true;

            offset = Int32.Parse(ViewState["offset"].ToString());
            ViewState["offset"] = (offset -= 10).ToString();

            page_count = Int32.Parse(ViewState["page_count"].ToString());
            ViewState["page_count"] = (--page_count).ToString();

            fillFeedsList(offset);

            if (page_count == 1)
                btn_feeds_prev.Enabled = false;

            drop_feedback_search.Focus();
        }


        //Search Feedback
        protected void btn_feedback_search_go_Click(object sender, EventArgs e)
        {
            //Set search params
            ViewState["ps_name"] = drop_feedback_search.SelectedItem.Text;
            ViewState["search_type"] = drop_feedback_search_options.SelectedItem.Value;

            //Trun on search mode
            search_mode = true;
            ViewState["search_mode"] = search_mode;

            //Reset offset and page count
            offset = 0;
            page_count = 1;
            ViewState["offset"] = offset.ToString();
            ViewState["page_count"] = page_count.ToString();
            btn_feeds_next.Enabled = false;
            btn_feeds_prev.Enabled = false;

            //Fill List
            fillFeedsList(offset);
        }


        //Revert from searched view
        protected void btn_feedback_revert_view_Click(object sender, EventArgs e)
        {
            Response.Redirect("ReadOnlyFeedbacks.aspx", true);
        }


        //Mark as Read or Unread
        protected void list_feedbacks_view_SelectedIndexChanging(object sender, ListViewSelectEventArgs e)
        {
            list_feedbacks_view.SelectedIndex = e.NewSelectedIndex;
                
            LinkButton lnk_mark_read_feed = new LinkButton();
            lnk_mark_read_feed = (LinkButton)list_feedbacks_view.Items[list_feedbacks_view.SelectedIndex].FindControl("lnk_mark_read_feed");

            //get PK and RK hidden fields
            HiddenField hidden_feed_pk = new HiddenField();
            hidden_feed_pk = (HiddenField)list_feedbacks_view.Items[list_feedbacks_view.SelectedIndex].FindControl("hidden_feed_pk");

            HiddenField hidden_feed_rk = new HiddenField();
            hidden_feed_rk = (HiddenField)list_feedbacks_view.Items[list_feedbacks_view.SelectedIndex].FindControl("hidden_feed_rk");

            //update Read stat
            ReportsObjectDataSource repDS = new ReportsObjectDataSource();
            if (lnk_mark_read_feed.Text == "Read") //Mark as Read
            {
                lnk_mark_read_feed.Text = "Unread";
                repDS.updateApproval(hidden_feed_pk.Value, hidden_feed_rk.Value, true);
            }
            else //Mark as Unread
            {
                lnk_mark_read_feed.Text = "Read";
                repDS.updateApproval(hidden_feed_pk.Value, hidden_feed_rk.Value, false);
            }

           

            //Remove display based on filter
            //search_mode = bool.Parse(ViewState["search_mode"].ToString());
            //if (search_mode)
            //{
            //    if ((drop_feedback_search_options.SelectedItem.Value == "1" && lnk_mark_read_feed.Text == "Unread") || 
            //        (drop_feedback_search_options.SelectedItem.Value == "2" && lnk_mark_read_feed.Text == "Read"))
            //        list_feedbacks_view.Items[list_feedbacks_view.SelectedIndex].Visible = false;
            //}

            //setLatLngs();

            offset = Int32.Parse(ViewState["offset"].ToString());
            fillFeedsList(offset);
        }


        //set latLng_list
        public void setLatLngs()
        {
            for (int i = 0; i < list_feedbacks_view.Items.Count; i++)
            {
                //lat
                Literal lit_feed_lat = new Literal();
                lit_feed_lat = (Literal)list_feedbacks_view.Items[i].FindControl("lit_feed_lat");

                //lng
                Literal lit_feed_long = new Literal();
                lit_feed_long = (Literal)list_feedbacks_view.Items[i].FindControl("lit_feed_long");


                latLng_List += lit_feed_lat.Text + "," + lit_feed_long.Text;
                if (i != list_feedbacks_view.Items.Count - 1)
                    latLng_List += "|";
            }
        }


        //Export feedback as .CSV
        protected void btn_export_feeds_Click(object sender, EventArgs e)
        {
            //Http Context
            HttpContext context = HttpContext.Current;
            context.Response.Clear();

            //Set Content type and header
            context.Response.ContentType = "text/csv";
            context.Response.AddHeader("Content-Disposition", "attachment; filename=Citizen_Feedbacks.csv");

            //Write Header Columns
            context.Response.Write("PS Id, PoliceStation, District, Range, Citizen Name, Phone, Email, Comments, Report Category, Latitude, Longitude, IP Address, Date Time, Read/Unread, Read Time, SHO Comment, SHO Comment Time, DCP Comment, DCP Comment Time, DCP Comment Public, DCP Non Public Comment, DCP Non Public Comment Time, SMS Sent, SMS Sent Time, SMS SHO Phone");
            context.Response.Write(Environment.NewLine);

            //Get Feedbacks and add rows
            ReportsObjectDataSource repDS = new ReportsObjectDataSource();
            IEnumerable<ReportsObjectEntity> repEnts = null;

            if (Session["level"].ToString() == "dcp")
                repEnts = (bool.Parse(ViewState["search_mode"].ToString())) ? repDS.getAllFilteredFeedbacksWithDS(Int32.Parse(Session["District_Id"].ToString()), ViewState["ps_name"].ToString(), ViewState["search_type"].ToString()) : repDS.getAllFeedbacksWithDS(Int32.Parse(Session["District_Id"].ToString()));
            else if (Session["level"].ToString() == "range")
                repEnts = (bool.Parse(ViewState["search_mode"].ToString())) ? repDS.getAllFilteredFeedbacksWithRange(Session["Range"].ToString(), ViewState["ps_name"].ToString(), ViewState["search_type"].ToString()) : repDS.getAllFeedbacksWithRange(Session["Range"].ToString());
            else
                repEnts = (bool.Parse(ViewState["search_mode"].ToString())) ? repDS.getAllFilteredFeedbacks(ViewState["ps_name"].ToString(), ViewState["search_type"].ToString()) : repDS.getAllFeedbacks();


            //Add CSV rows
            foreach (ReportsObjectEntity repEnt in repEnts)
            {
                //PS Id
                context.Response.Write(repEnt.PartitionKey + ",");
                //PoliceStation
                context.Response.Write("\"" + repEnt.policestation + "\",");

                //Have to get District and Range
                var distEntObj = repDS.getDistrictFromPsId(repEnt.PartitionKey);
                foreach (DistrictEntity distEnt in distEntObj)
                {
                    //District
                    context.Response.Write("\"" + distEnt.name + "\",");
                    //Range
                    context.Response.Write("\"" + distEnt.districtrange + "\",");
                }

                //Citizen Name
                context.Response.Write("\"" + repEnt.name + "\",");
                //Phone
                context.Response.Write("\"" + repEnt.phone + "\",");
                //Email
                context.Response.Write("\"" + repEnt.email + "\",");
                //Comments
                context.Response.Write("\"" + repEnt.details + "\",");
                //Report Category
                context.Response.Write("\"" + repEnt.category + "\",");
                //Latitude
                context.Response.Write(repEnt.latitude + ",");
                //Longitude
                context.Response.Write(repEnt.longitude + ",");
                //IP Address
                context.Response.Write(repEnt.ipaddress + ",");
                //Date Time
                context.Response.Write("\"" + repEnt.date_time + "\",");
                //Read/Unread
                if (repEnt.approval_stat == true)
                {
                    context.Response.Write("\"Read\",");
                    //Read Time
                    context.Response.Write("\"" + repEnt.read_time.ToString() + "\",");
                }
                else
                {
                    context.Response.Write("\"Unread\",");
                    //Read Time
                    context.Response.Write(",");
                }

                //SHO Reply
                if (repEnt.sho_reply != null && repEnt.sho_reply != "")
                {
                    context.Response.Write("\"" + repEnt.sho_reply + "\",");
                    //SHO Reply Time
                    context.Response.Write("\"" + repEnt.sho_reply_time + "\",");
                }
                else
                {
                    context.Response.Write(",");
                    //SHO Reply Time
                    context.Response.Write(",");
                }

                //DCP Reply
                if (repEnt.dcp_reply != null && repEnt.dcp_reply != "")
                {
                    context.Response.Write("\"" + repEnt.dcp_reply + "\",");
                    //DCP Reply Time
                    context.Response.Write("\"" + repEnt.dcp_reply_time + "\",");
                    //DCP Reply Public
                    if (repEnt.dcp_reply_public == true)
                        context.Response.Write("\"Yes\",");
                    else
                        context.Response.Write("\"No\",");
                }
                else
                {
                    context.Response.Write(",");
                    //DCP Reply Time
                    context.Response.Write(",");
                    //DCP Reply Public
                    context.Response.Write(",");
                }

                //DCP Non Public Reply
                if (repEnt.dcp_non_public_reply != null && repEnt.dcp_non_public_reply != "")
                {
                    context.Response.Write("\"" + repEnt.dcp_non_public_reply + "\",");
                    //DCP Non Public Reply Time
                    context.Response.Write("\"" + repEnt.dcp_non_public_reply_time + "\",");
                }
                else
                {
                    context.Response.Write(",");
                    //DCP Non Public Reply Time
                    context.Response.Write(",");
                }


                //SMS Sent
                if (repEnt.sms_sent == true)
                {
                    context.Response.Write("\"Yes\",");
                    //SMS Sent Time
                    context.Response.Write("\"" + repEnt.sms_send_time.Value + "\",");
                    //SMS SHO Phone
                    context.Response.Write("\"" + repEnt.sms_sho_phone + "\"");
                }
                else
                {
                    context.Response.Write("\"No\",");
                    //SMS Sent Time
                    context.Response.Write(",");
                    //SMS SHO Phone
                    context.Response.Write("");
                }


                //NewLine
                context.Response.Write(Environment.NewLine);
            }

            //End of file
            context.Response.End();
        }


        //DCP Reply
        protected void list_feedbacks_view_ItemEditing(object sender, ListViewEditEventArgs e)
        {
            //get PK
            HiddenField hidden_feed_pk = new HiddenField();
            hidden_feed_pk = (HiddenField)list_feedbacks_view.Items[e.NewEditIndex].FindControl("hidden_feed_pk");
            Session["Reports_PK"] = hidden_feed_pk.Value;

            //get Rk
            HiddenField hidden_feed_rk = new HiddenField();
            hidden_feed_rk = (HiddenField)list_feedbacks_view.Items[e.NewEditIndex].FindControl("hidden_feed_rk");
            Session["Reports_RK"] = hidden_feed_rk.Value;

            Response.Redirect("DcpReply.aspx", true);
        }

    }
}