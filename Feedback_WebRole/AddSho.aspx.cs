﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using CitizenReports;

namespace Feedback_WebRole
{
    public partial class AddSho : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["logged_in"] == null)
                Response.Redirect("Login.aspx", true);
        }


        //Add New SHO
        protected void btn_add_new_sho_Click(object sender, EventArgs e)
        {
            if (file_add_sho_photo.HasFile)
            {
                string extension = Path.GetExtension(file_add_sho_photo.FileName);
                if (extension != ".jpg")
                {
                    lbl_add_sho_rsp.Text = "Invalid file type!";
                    return;
                }
            }

            if (txt_add_sho_id.Text == "" || txt_add_sho_name.Text == "")
                return;

            OfficerObject officerObj = new OfficerObject();
            officerObj.id = txt_add_sho_id.Text;
            officerObj.name = txt_add_sho_name.Text;

            OfficerObjectDataSource officerDS = new OfficerObjectDataSource();
            officerDS.putOfficer(officerObj);


            if (file_add_sho_photo.HasFile)
            {
                //upload blob
                officerDS.putShoPhoto(txt_add_sho_id.Text, file_add_sho_photo.FileContent);
            }


            lbl_add_sho_rsp.Text = "New SHO added successfully";

            txt_add_sho_id.Text = "";
            txt_add_sho_name.Text = "";
        }

        protected void btn_back2sho_Click(object sender, EventArgs e)
        {
            Response.Redirect("StationOfficers.aspx");
        }
    }
}