﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CitizenReports;

namespace Feedback_WebRole
{
    public partial class EditCrime : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["logged_in"] == null)
                Response.Redirect("Login.aspx", true);

            if (!IsPostBack)
            {
                //fill edit fields
                fillEditCrime(Session["Crime_PK"].ToString(), Session["Crime_RK"].ToString());
            }
        }


        //fill edit fields
        public void fillEditCrime(string partitionKey, string rowKey)
        {
            CrimeObjectDataSource crimeDS = new CrimeObjectDataSource();

            CrimeObjectEntity crimeObj = new CrimeObjectEntity();
            crimeObj = crimeDS.getACrime(partitionKey, rowKey);

            //get All PS List for dropdown
            PSObjectDataSource psDS = new PSObjectDataSource();
            drop_ps_list.DataSource = psDS.getAllPoliceStations();
            drop_ps_list.DataTextField = "RowKey";
            drop_ps_list.DataValueField = "RowKey";
            drop_ps_list.DataBind();

            //set dropdown index for selected PS ID
            for (int i = 0; i < drop_ps_list.Items.Count; i++)
            {
                string currentValue = drop_ps_list.Items[i].Value;
                if (currentValue == partitionKey) //partiton key = PS ID
                {
                    drop_ps_list.SelectedIndex = i;
                    break;
                }
            }


            txt_incident_count.Text = crimeObj.incident_count.ToString();
            txt_district_id.Text = crimeObj.district_id.ToString();
            txt_crime_head.Text = crimeObj.crime_head;
        }


        //Update Crime
        protected void btn_update_crime_Click(object sender, EventArgs e)
        {
            btn_update_crime.Enabled = false;
            lbl_crime_response.Text = "Please wait...";

            CrimeObject crimeObj = new CrimeObject();

            crimeObj.ps_id = Int32.Parse(drop_ps_list.SelectedItem.Value);
            crimeObj.incident_count = Int32.Parse(txt_incident_count.Text);
            crimeObj.district_id = Int32.Parse(txt_district_id.Text);
            crimeObj.crime_head = txt_crime_head.Text;

            CrimeObjectDataSource crimeDS = new CrimeObjectDataSource();
            crimeDS.updateCrime(crimeObj, Session["Crime_PK"].ToString(), Session["Crime_RK"].ToString());

            lbl_crime_response.Text = "Updated successfully.";
            btn_update_crime.Enabled = true;
        }


        //Back to Crime
        protected void btn_backToCrime_Click(object sender, EventArgs e)
        {
            Response.Redirect("Crimes.aspx", true);
        }
    }
}