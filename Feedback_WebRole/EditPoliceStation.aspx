﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditPoliceStation.aspx.cs" Inherits="Feedback_WebRole.EditPoliceStation" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 300px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Edit Police Station Details</h2>

    <table class="style1">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Station Name"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_station" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="SHO ID"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="drop_sho_ids" runat="server" Height="16px" Width="99px">
                </asp:DropDownList>
                <asp:ListSearchExtender ID="drop_sho_ids_ListSearchExtender" runat="server" 
                    Enabled="True" QueryPattern="Contains" TargetControlID="drop_sho_ids">
                </asp:ListSearchExtender>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="SHO Phone"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_shophone" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="SHO Mobile"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_shomobile" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label5" runat="server" Text="SHO Email"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_shoemail" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Latitude"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_lat" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label7" runat="server" Text="Longitude"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_long" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Label ID="Label8" runat="server" Text="Address"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_address" runat="server" Height="86px" TextMode="MultiLine" 
                    Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_updatePS" runat="server" onclick="btn_updatePS_Click" 
                    Text="Update" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_ps_response" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <p>
        <asp:Button ID="btn_backToPS" runat="server" onclick="btn_backToPS_Click" 
            Text="&lt;&lt; Back" />
    </p>
    <p>
        &nbsp;</p>
</asp:Content>
