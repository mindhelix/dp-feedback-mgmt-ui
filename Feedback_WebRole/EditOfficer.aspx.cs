﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using CitizenReports;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.StorageClient;

namespace Feedback_WebRole
{
    public partial class EditOfficer : System.Web.UI.Page
    {
        OfficerObjectDataSource officerDS = new OfficerObjectDataSource();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["logged_in"] == null)
                Response.Redirect("Login.aspx", true);

            if (!IsPostBack)
            {
                //get shoid and PK for edit values
                var sho_id = Session["SHO_ID"].ToString();
                var officer_pk = Session["OFFICER_PK"].ToString();

                var OfficerEntObj = officerDS.getAOfficer(sho_id, officer_pk);

                foreach (OfficerObjectEntity officerEnt in OfficerEntObj)
                {
                    txt_sho_id.Text = officerEnt.id.ToString();
                    txt_sho_name.Text = officerEnt.name;
                }

                //SHO Photo
                showShoPhoto(sho_id);
            }
        }


        //show SHO Photo for edit
        public void showShoPhoto(string sho_id)
        {
            CloudBlob blob = officerDS.getAShoPhoto(sho_id);

            try
            {
                blob.FetchAttributes();
                img_officer_photo.ImageUrl = blob.Uri.ToString();
                //set ctrls visibility
                fileup_sho_photo.Visible = false;
                btn_upload_sho_photo.Visible = false;
                lbl_sho_pic_rsp.Visible = false;
                lnk_del_sho_pic.Visible = true;
            }
            catch (StorageClientException sce)
            {
                img_officer_photo.ImageUrl = "Images/empty-profile.png";

                //set ctrls visibility
                fileup_sho_photo.Visible = true;
                btn_upload_sho_photo.Visible = true;
                lbl_sho_pic_rsp.Visible = true;
                lnk_del_sho_pic.Visible = false;
            }
        }


        //Update Officer Details
        protected void btn_update_officer_Click(object sender, EventArgs e)
        {
            OfficerObject officerObj = new OfficerObject();
            officerObj.id = txt_sho_id.Text;
            officerObj.name = txt_sho_name.Text;

            //check whether the id already exists
            if (officerDS.chkOfficerID(Session["SHO_ID"].ToString(), txt_sho_id.Text))
            {
                lbl_officer_update_rsp.Text = "The entered SHO ID already exists";
                return;
            }

            //update call
            officerDS.updateOfficer(Session["SHO_ID"].ToString(), Session["OFFICER_PK"].ToString(), officerObj);

            //update sho photo id
            officerDS.updateShoPhotoID(Session["SHO_ID"].ToString(), txt_sho_id.Text);

            lbl_officer_update_rsp.Text = "Officer details updated.";

            Session["SHO_ID"] = txt_sho_id.Text;

            //SHO Photo
            showShoPhoto(txt_sho_id.Text);
        }


        //Upload SHO Photo
        protected void btn_upload_sho_photo_Click(object sender, EventArgs e)
        {
            if (fileup_sho_photo.HasFile)
            {
                string extension = Path.GetExtension(fileup_sho_photo.FileName);
                if (extension != ".jpg")
                {
                    lbl_sho_pic_rsp.Text = "Invalid file type!";
                    return;
                }

                

                //upload blob
                officerDS.putShoPhoto(Session["SHO_ID"].ToString(), fileup_sho_photo.FileContent);

                lbl_sho_pic_rsp.Text = "SHO Photo updated.";

                //SHO Photo Redisplay
                showShoPhoto(Session["SHO_ID"].ToString());
            }
        }

        protected void lnk_del_sho_pic_Click(object sender, EventArgs e)
        {
            //delete blob
            officerDS.deleteShoPhoto(Session["SHO_ID"].ToString());
            //SHO Photo Redisplay
            showShoPhoto(Session["SHO_ID"].ToString());
        }

        protected void btn_backFromShoEdit_Click(object sender, EventArgs e)
        {
            Response.Redirect("StationOfficers.aspx");
        }
    }
}