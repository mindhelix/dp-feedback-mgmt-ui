﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CitizenReports;
using System.Security.Cryptography;
using System.Text;

namespace Feedback_WebRole
{
    public partial class UserControl : System.Web.UI.Page
    {
        UserObjectDataSource userDS = new UserObjectDataSource();
        UserDataDataSource userDataDS = new UserDataDataSource();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["logged_in"] == null)
                Response.Redirect("Login.aspx", true);

            if (!IsPostBack)
            {
                //DCP Users
                grid_dcp_user_list.DataSource = userDS.GetUsersByLevel(2);
                grid_dcp_user_list.DataBind();
                //set_district_selection
                set_district_selection();

                
                //Range Users
                grid_range_users_list.DataSource = userDS.GetUsersByLevel(3);
                grid_range_users_list.DataBind();
                //set Range selection
                set_range_selection();


                //Read Only users
                grid_read_only.DataSource = userDS.GetUsersByLevel(10);
                grid_read_only.DataBind();

                //PHQ users
                grid_phq_users.DataSource = userDS.GetUsersByLevel(4);
                grid_phq_users.DataBind();
            }
        }

        //Set District Selection in Grid
        public void set_district_selection()
        {
            for (int i = 0; i < grid_dcp_user_list.Rows.Count; i++)
            {
                var user_id = grid_dcp_user_list.DataKeys[i].Values["RowKey"].ToString();
                //get DCP's District
                UserDataDataSource userDataDS = new UserDataDataSource();
                UserDataEntity userData = userDataDS.GetUserData(user_id, "District_Id");
                var district_id = userData.data_value;

                //District Dropdown
                DropDownList drop_districts = new DropDownList();
                drop_districts = (DropDownList)grid_dcp_user_list.Rows[i].Cells[3].FindControl("drop_districts");
                for (int j = 0; j < drop_districts.Items.Count; j++)
                {
                    if (drop_districts.Items[j].Value == district_id)
                    {
                        drop_districts.Items[j].Selected = true;
                        break;
                    }
                }
            }
        }


        //Set Range Selection
        public void set_range_selection()
        {
            for (int i = 0; i < grid_range_users_list.Rows.Count; i++)
            {
                var user_id = grid_range_users_list.DataKeys[i].Values["RowKey"].ToString();
                //get RangeUser's Range
                UserDataDataSource userDataDS = new UserDataDataSource();
                UserDataEntity userData = userDataDS.GetUserData(user_id, "Range");
                var range = userData.data_value;

                //District Dropdown
                DropDownList drop_ranges = new DropDownList();
                drop_ranges = (DropDownList)grid_range_users_list.Rows[i].Cells[3].FindControl("drop_ranges");
                for (int j = 0; j < drop_ranges.Items.Count; j++)
                {
                    if (drop_ranges.Items[j].Value == range)
                    {
                        drop_ranges.Items[j].Selected = true;
                        break;
                    }
                }
            }
        }


        //Add District DCP user
        protected void btn_add_dcp_user_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddDcpUser.aspx", true);
        }

        //Add Range User
        protected void btn_add_range_user_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddRangeUser.aspx", true);
        }

        //Add Read Only User -> Law & Order User
        protected void btn_add_read_only_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddReadOnlyUser.aspx", true);
        }


        //Delete DCP User
        protected void grid_dcp_user_list_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
             //get User Id
            var user_id = grid_dcp_user_list.DataKeys[e.RowIndex].Values["RowKey"].ToString();

            //Delete User
            UserObjectDataSource userDS = new UserObjectDataSource();
            userDS.DelUser(user_id);

            //Delete User Data
            UserDataDataSource userDataDS = new UserDataDataSource();
            userDataDS.DelAllUserData(user_id);

            //Rebind Grid
            //DCP Users
            grid_dcp_user_list.DataSource = userDS.GetUsersByLevel(2);
            grid_dcp_user_list.DataBind();
            //set_district_selection
            set_district_selection();
        }

        


        //Delete Range User
        protected void grid_range_users_list_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //get User Id
            var user_id = grid_range_users_list.DataKeys[e.RowIndex].Values["RowKey"].ToString();

            //Delete User
            UserObjectDataSource userDS = new UserObjectDataSource();
            userDS.DelUser(user_id);

            //Delete User Data
            UserDataDataSource userDataDS = new UserDataDataSource();
            userDataDS.DelAllUserData(user_id);

            //Rebind Grid
            //Range Users
            grid_range_users_list.DataSource = userDS.GetUsersByLevel(3);
            grid_range_users_list.DataBind();
            //set Range selection
            set_range_selection();
        }


        //Delete Read-Only / Law & Order User
        protected void grid_read_only_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //get User Id
            var user_id = grid_read_only.DataKeys[e.RowIndex].Values["RowKey"].ToString();

            //Delete User
            UserObjectDataSource userDS = new UserObjectDataSource();
            userDS.DelUser(user_id);

            //Rebind Grid
            //Read Only users
            grid_read_only.DataSource = userDS.GetUsersByLevel(10);
            grid_read_only.DataBind();
        }


        //DCP User Editing
        protected void grid_dcp_user_list_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grid_dcp_user_list.EditIndex = e.NewEditIndex;
            
            //Rebind Grid
            //DCP Users
            grid_dcp_user_list.DataSource = userDS.GetUsersByLevel(2);
            grid_dcp_user_list.DataBind();
            //set_district_selection
            set_district_selection();


            //Set Edit fields

            //Show and Enable New Password Field
            grid_dcp_user_list.Columns[2].Visible = true;
            TextBox txt_dcp_new_pass = new TextBox();
            txt_dcp_new_pass = (TextBox)grid_dcp_user_list.Rows[e.NewEditIndex].Cells[2].FindControl("txt_dcp_new_pass");
            txt_dcp_new_pass.Visible = true;

            //Enable District Field
            DropDownList drop_districts = new DropDownList();
            drop_districts = (DropDownList)grid_dcp_user_list.Rows[e.NewEditIndex].Cells[3].FindControl("drop_districts");
            drop_districts.Enabled = true;

            //Disable Username field
            grid_dcp_user_list.Rows[e.NewEditIndex].Cells[1].Enabled = false;
        }


        //Cancel DCP Edit
        protected void grid_dcp_user_list_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grid_dcp_user_list.EditIndex = -1;

            //Rebind Grid
            //DCP Users
            grid_dcp_user_list.DataSource = userDS.GetUsersByLevel(2);
            grid_dcp_user_list.DataBind();
            //set_district_selection
            set_district_selection();


            //Hide Password field
            grid_dcp_user_list.Columns[2].Visible = false;
        }


        //DCP User update
        protected void grid_dcp_user_list_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            //get User Id
            var user_id = grid_dcp_user_list.DataKeys[e.RowIndex].Values["RowKey"].ToString();

            //get New Password
            TextBox txt_dcp_new_pass = new TextBox();
            txt_dcp_new_pass = (TextBox)grid_dcp_user_list.Rows[e.RowIndex].Cells[2].FindControl("txt_dcp_new_pass");
            
            if (txt_dcp_new_pass.Text != "") //Update
            {
                //Compute MD5 of password
                Byte[] originalBytes;
                Byte[] encodedBytes;
                MD5 md5 = new MD5CryptoServiceProvider();

                originalBytes = ASCIIEncoding.Default.GetBytes(txt_dcp_new_pass.Text);
                encodedBytes = md5.ComputeHash(originalBytes);

                var new_encrypted_pass = BitConverter.ToString(encodedBytes);

                userDS.UpdatePassword(user_id, new_encrypted_pass);
            }


            //Get and Update District
            DropDownList drop_districts = new DropDownList();
            drop_districts = (DropDownList)grid_dcp_user_list.Rows[e.RowIndex].Cells[3].FindControl("drop_districts");

            userDataDS.UpdateUserData(user_id, "District_Id", drop_districts.SelectedItem.Value);


            //Finish Edit Mode
            grid_dcp_user_list.EditIndex = -1;

            //Rebind Grid
            //DCP Users
            grid_dcp_user_list.DataSource = userDS.GetUsersByLevel(2);
            grid_dcp_user_list.DataBind();
            //set_district_selection
            set_district_selection();


            //Hide Password field
            grid_dcp_user_list.Columns[2].Visible = false;
        }


        //Edit Range User
        protected void grid_range_users_list_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grid_range_users_list.EditIndex = e.NewEditIndex;

            //Rebind
            //Range Users
            grid_range_users_list.DataSource = userDS.GetUsersByLevel(3);
            grid_range_users_list.DataBind();
            //set Range selection
            set_range_selection();


            //Set Edit fields

            //Show and Enable New Password Field
            grid_range_users_list.Columns[2].Visible = true;
            TextBox txt_range_user_pass = new TextBox();
            txt_range_user_pass = (TextBox)grid_range_users_list.Rows[e.NewEditIndex].Cells[2].FindControl("txt_range_user_pass");
            txt_range_user_pass.Visible = true;

            //Enable District Field
            DropDownList drop_ranges = new DropDownList();
            drop_ranges = (DropDownList)grid_range_users_list.Rows[e.NewEditIndex].Cells[3].FindControl("drop_ranges");
            drop_ranges.Enabled = true;

            //Disable Username field
            grid_range_users_list.Rows[e.NewEditIndex].Cells[1].Enabled = false;
        }

        //Cancel Range User Edit
        protected void grid_range_users_list_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grid_range_users_list.EditIndex = -1;

            //Rebind
            //Range Users
            grid_range_users_list.DataSource = userDS.GetUsersByLevel(3);
            grid_range_users_list.DataBind();
            //set Range selection
            set_range_selection();


            //Hide Password field
            grid_range_users_list.Columns[2].Visible = false;
        }


        //Range User Update
        protected void grid_range_users_list_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            //get User Id
            var user_id = grid_range_users_list.DataKeys[e.RowIndex].Values["RowKey"].ToString();

            //get New Password
            TextBox txt_range_user_pass = new TextBox();
            txt_range_user_pass = (TextBox)grid_range_users_list.Rows[e.RowIndex].Cells[2].FindControl("txt_range_user_pass");

            if (txt_range_user_pass.Text != "") //Update
            {
                //Compute MD5 of password
                Byte[] originalBytes;
                Byte[] encodedBytes;
                MD5 md5 = new MD5CryptoServiceProvider();

                originalBytes = ASCIIEncoding.Default.GetBytes(txt_range_user_pass.Text);
                encodedBytes = md5.ComputeHash(originalBytes);

                var new_encrypted_pass = BitConverter.ToString(encodedBytes);

                userDS.UpdatePassword(user_id, new_encrypted_pass);
            }


            //Get and Update Range
            DropDownList drop_ranges = new DropDownList();
            drop_ranges = (DropDownList)grid_range_users_list.Rows[e.RowIndex].Cells[3].FindControl("drop_ranges");

            userDataDS.UpdateUserData(user_id, "Range", drop_ranges.SelectedItem.Value);


            //Finish Edit Mode
            grid_range_users_list.EditIndex = -1;

            //Rebind
            //Range Users
            grid_range_users_list.DataSource = userDS.GetUsersByLevel(3);
            grid_range_users_list.DataBind();
            //set Range selection
            set_range_selection();


            //Hide Password field
            grid_range_users_list.Columns[2].Visible = false;
        }


        //Read User Edit
        protected void grid_read_only_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grid_read_only.EditIndex = e.NewEditIndex;

            //Rebind
            //Read Only users
            grid_read_only.DataSource = userDS.GetUsersByLevel(10);
            grid_read_only.DataBind();


            //Set Edit fields

            //Show and Enable New Password Field
            grid_read_only.Columns[2].Visible = true;
            TextBox txt_read_user_pass = new TextBox();
            txt_read_user_pass = (TextBox)grid_read_only.Rows[e.NewEditIndex].Cells[2].FindControl("txt_read_user_pass");
            txt_read_user_pass.Visible = true;

            //Disable Username field
            grid_read_only.Rows[e.NewEditIndex].Cells[1].Enabled = false;
        }

        //Cancel Read User Edit
        protected void grid_read_only_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grid_read_only.EditIndex = -1;

            //Rebind
            //Read Only users
            grid_read_only.DataSource = userDS.GetUsersByLevel(10);
            grid_read_only.DataBind();


            //Hide Password field
            grid_read_only.Columns[2].Visible = false;
        }


        //Update Read Only User
        protected void grid_read_only_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            //get User Id
            var user_id = grid_read_only.DataKeys[e.RowIndex].Values["RowKey"].ToString();

            //get New Password
            TextBox txt_read_user_pass = new TextBox();
            txt_read_user_pass = (TextBox)grid_read_only.Rows[e.RowIndex].Cells[2].FindControl("txt_read_user_pass");

            if (txt_read_user_pass.Text != "") //Update
            {
                //Compute MD5 of password
                Byte[] originalBytes;
                Byte[] encodedBytes;
                MD5 md5 = new MD5CryptoServiceProvider();

                originalBytes = ASCIIEncoding.Default.GetBytes(txt_read_user_pass.Text);
                encodedBytes = md5.ComputeHash(originalBytes);

                var new_encrypted_pass = BitConverter.ToString(encodedBytes);

                userDS.UpdatePassword(user_id, new_encrypted_pass);
            }


            //Finish Edit Mode
            grid_read_only.EditIndex = -1;

            //Rebind
            //Read Only users
            grid_read_only.DataSource = userDS.GetUsersByLevel(10);
            grid_read_only.DataBind();


            //Hide Password field
            grid_read_only.Columns[2].Visible = false;
        }


        //PHQ User Delete
        protected void grid_phq_users_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //get User Id
            var user_id = grid_phq_users.DataKeys[e.RowIndex].Values["RowKey"].ToString();

            //Delete User
            UserObjectDataSource userDS = new UserObjectDataSource();
            userDS.DelUser(user_id);

            //Rebind Grid
            //PHQ users
            grid_phq_users.DataSource = userDS.GetUsersByLevel(4);
            grid_phq_users.DataBind();
        }


        //Add PHQ User
        protected void btn_phq_user_add_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddPhqUser.aspx", true);
        }


        //Edit PHQ User
        protected void grid_phq_users_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grid_phq_users.EditIndex = e.NewEditIndex;

            //Rebind
            //PHQ users
            grid_phq_users.DataSource = userDS.GetUsersByLevel(4);
            grid_phq_users.DataBind();


            //Set Edit fields

            //Show and Enable New Password Field
            grid_phq_users.Columns[2].Visible = true;
            TextBox txt_phq_user_pass = new TextBox();
            txt_phq_user_pass = (TextBox)grid_phq_users.Rows[e.NewEditIndex].Cells[2].FindControl("txt_phq_user_pass");
            txt_phq_user_pass.Visible = true;

            //Disable Username field
            grid_phq_users.Rows[e.NewEditIndex].Cells[1].Enabled = false;
        }


        //Canel PHQ User Edit
        protected void grid_phq_users_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grid_phq_users.EditIndex = -1;

            //Rebind
            //PHQ users
            grid_phq_users.DataSource = userDS.GetUsersByLevel(4);
            grid_phq_users.DataBind();


            //Hide Password field
            grid_phq_users.Columns[2].Visible = false;
        }


        //Update PHQ User
        protected void grid_phq_users_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            //get User Id
            var user_id = grid_phq_users.DataKeys[e.RowIndex].Values["RowKey"].ToString();

            //get New Password
            TextBox txt_phq_user_pass = new TextBox();
            txt_phq_user_pass = (TextBox)grid_phq_users.Rows[e.RowIndex].Cells[2].FindControl("txt_phq_user_pass");

            if (txt_phq_user_pass.Text != "") //Update
            {
                //Compute MD5 of password
                Byte[] originalBytes;
                Byte[] encodedBytes;
                MD5 md5 = new MD5CryptoServiceProvider();

                originalBytes = ASCIIEncoding.Default.GetBytes(txt_phq_user_pass.Text);
                encodedBytes = md5.ComputeHash(originalBytes);

                var new_encrypted_pass = BitConverter.ToString(encodedBytes);

                userDS.UpdatePassword(user_id, new_encrypted_pass);
            }


            //Finish Edit Mode
            grid_phq_users.EditIndex = -1;

            //Rebind
            //PHQ users
            grid_phq_users.DataSource = userDS.GetUsersByLevel(4);
            grid_phq_users.DataBind();


            //Hide Password field
            grid_phq_users.Columns[2].Visible = false;
        }

      
    }
}