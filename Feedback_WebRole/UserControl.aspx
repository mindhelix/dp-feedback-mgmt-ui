﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserControl.aspx.cs" Inherits="Feedback_WebRole.UserControl" MaintainScrollPositionOnPostback="true" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 762px;
        }
        .style3
        {
            width: 156px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>User Management</h2>
    <h3>DCP Users List</h3>

    <table class="style1">
        <tr>
            <td>
                
                <asp:GridView ID="grid_dcp_user_list" runat="server" 
                    AutoGenerateColumns="False" CellPadding="4" DataKeyNames="RowKey" 
                    ForeColor="#333333" GridLines="None" 
                    EmptyDataText="No DCP users are added." 
                    onrowdeleting="grid_dcp_user_list_RowDeleting" 
                    onrowcancelingedit="grid_dcp_user_list_RowCancelingEdit" 
                    onrowediting="grid_dcp_user_list_RowEditing" 
                    onrowupdating="grid_dcp_user_list_RowUpdating">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:BoundField DataField="RowKey" HeaderText="user_id/RowKey" 
                            Visible="False" />
                        <asp:BoundField DataField="Username" HeaderText="Username" />
                        <asp:TemplateField HeaderText="New Password" Visible="False">
                            <ItemTemplate>
                                <asp:TextBox ID="txt_dcp_new_pass" runat="server" Visible="False"></asp:TextBox>
                                <asp:TextBoxWatermarkExtender ID="txt_dcp_new_pass_TextBoxWatermarkExtender" 
                                    runat="server" Enabled="True" TargetControlID="txt_dcp_new_pass" 
                                    WatermarkText="New Password">
                                </asp:TextBoxWatermarkExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="District">
                            <ItemTemplate>
                                <asp:DropDownList ID="drop_districts" runat="server" 
                                    DataSourceID="ObjectDataSource1" DataTextField="name" DataValueField="id" 
                                    Enabled="False">
                                </asp:DropDownList>
                                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                                    SelectMethod="getAllDistricts" TypeName="CitizenReports.DistrictDataSource">
                                </asp:ObjectDataSource>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowEditButton="True" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnk_del_dcp" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                                <asp:ConfirmButtonExtender ID="lnk_del_dcp_ConfirmButtonExtender" 
                                    runat="server" ConfirmOnFormSubmit="True" 
                                    ConfirmText="Do you really want to delete?" Enabled="True" 
                                    TargetControlID="lnk_del_dcp">
                                </asp:ConfirmButtonExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
                
                <asp:DropShadowExtender ID="grid_dcp_user_list_DropShadowExtender" 
                    runat="server" Enabled="True" Opacity="0.5" 
                    TargetControlID="grid_dcp_user_list">
                </asp:DropShadowExtender>
                
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btn_add_dcp_user" runat="server" 
                    onclick="btn_add_dcp_user_Click" Text="Add DCP" />
            </td>
        </tr>
        </table>

        <hr />

    <h3>Range Users List</h3>

    <table class="style1">
        <tr>
            <td>
                <asp:GridView ID="grid_range_users_list" runat="server" 
                    AutoGenerateColumns="False" CellPadding="4" DataKeyNames="RowKey" 
                    EmptyDataText="No Range users are added." ForeColor="#333333" 
                    GridLines="None" onrowdeleting="grid_range_users_list_RowDeleting" 
                    onrowcancelingedit="grid_range_users_list_RowCancelingEdit" 
                    onrowediting="grid_range_users_list_RowEditing" 
                    onrowupdating="grid_range_users_list_RowUpdating">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:BoundField DataField="RowKey" HeaderText="user_id/RowKey" 
                            Visible="False" />
                        <asp:BoundField DataField="Username" HeaderText="Username" />
                        <asp:TemplateField HeaderText="New Password" Visible="False">
                            <ItemTemplate>
                                <asp:TextBox ID="txt_range_user_pass" runat="server" Visible="False"></asp:TextBox>
                                <asp:TextBoxWatermarkExtender ID="txt_range_user_pass_TextBoxWatermarkExtender" 
                                    runat="server" Enabled="True" TargetControlID="txt_range_user_pass" 
                                    WatermarkText="New Password">
                                </asp:TextBoxWatermarkExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Range">
                            <ItemTemplate>
                                <asp:DropDownList ID="drop_ranges" runat="server" 
                                    DataSourceID="ObjectDataSource2" DataTextField="districtrange" 
                                    DataValueField="PartitionKey" Enabled="False">
                                </asp:DropDownList>
                                <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
                                    SelectMethod="getAllRanges" TypeName="CitizenReports.DistrictDataSource">
                                </asp:ObjectDataSource>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowEditButton="True" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnk_del_range_user" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                                <asp:ConfirmButtonExtender ID="lnk_del_range_user_ConfirmButtonExtender" 
                                    runat="server" ConfirmOnFormSubmit="True" 
                                    ConfirmText="Do you really want to delete?" Enabled="True" 
                                    TargetControlID="lnk_del_range_user">
                                </asp:ConfirmButtonExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
                <asp:DropShadowExtender ID="grid_range_users_list_DropShadowExtender" 
                    runat="server" Enabled="True" Opacity="0.5" 
                    TargetControlID="grid_range_users_list">
                </asp:DropShadowExtender>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btn_add_range_user" runat="server" 
                    onclick="btn_add_range_user_Click" Text="Add Range User" 
                    style="height: 26px" />
            </td>
        </tr>
        </table>

        <hr />

    <h3>Law & Order Users List</h3>
    <table class="style1">
        <tr>
            <td>
                <asp:GridView ID="grid_read_only" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" EmptyDataText="No Law &amp; Order users are added." 
                    ForeColor="#333333" GridLines="None" 
                    onrowdeleting="grid_read_only_RowDeleting" DataKeyNames="RowKey" 
                    onrowcancelingedit="grid_read_only_RowCancelingEdit" 
                    onrowediting="grid_read_only_RowEditing" 
                    onrowupdating="grid_read_only_RowUpdating">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:BoundField DataField="RowKey" HeaderText="user_id/RowKey" 
                            Visible="False" />
                        <asp:BoundField DataField="Username" HeaderText="Username" />
                        <asp:TemplateField HeaderText="New Password" Visible="False">
                            <ItemTemplate>
                                <asp:TextBox ID="txt_read_user_pass" runat="server" Visible="False"></asp:TextBox>
                                <asp:TextBoxWatermarkExtender ID="txt_read_user_pass_TextBoxWatermarkExtender" 
                                    runat="server" Enabled="True" TargetControlID="txt_read_user_pass" 
                                    WatermarkText="New Password">
                                </asp:TextBoxWatermarkExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowEditButton="True" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnk_del_read_only" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                                <asp:ConfirmButtonExtender ID="lnk_del_read_only_ConfirmButtonExtender" 
                                    runat="server" ConfirmOnFormSubmit="True" 
                                    ConfirmText="Do you really want to delete?" Enabled="True" 
                                    TargetControlID="lnk_del_read_only">
                                </asp:ConfirmButtonExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
                <asp:DropShadowExtender ID="grid_read_only_DropShadowExtender" runat="server" 
                    Enabled="True" Opacity="0.5" TargetControlID="grid_read_only">
                </asp:DropShadowExtender>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btn_add_read_only" runat="server" 
                    onclick="btn_add_read_only_Click" Text="Add Law &amp; Order User" />
            </td>
        </tr>
    </table>

    <hr />

    <h3>PHQ Users List</h3>

    <table class="style1">
        <tr>
            <td>
                <asp:GridView ID="grid_phq_users" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" DataKeyNames="RowKey" 
                    EmptyDataText="No PHQ users are added." 
                    onrowcancelingedit="grid_phq_users_RowCancelingEdit" 
                    onrowdeleting="grid_phq_users_RowDeleting" 
                    onrowediting="grid_phq_users_RowEditing" 
                    onrowupdating="grid_phq_users_RowUpdating">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:BoundField DataField="RowKey" HeaderText="user_id/RowKey" 
                            Visible="False" />
                        <asp:BoundField DataField="Username" HeaderText="Username" />
                        <asp:TemplateField HeaderText="New Password" Visible="False">
                            <ItemTemplate>
                                <asp:TextBox ID="txt_phq_user_pass" runat="server" Visible="False"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowEditButton="True" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnk_phq_user_del" runat="server" CommandName="Delete">Delete</asp:LinkButton>
                                <asp:ConfirmButtonExtender ID="lnk_phq_user_del_ConfirmButtonExtender" 
                                    runat="server" ConfirmOnFormSubmit="True" 
                                    ConfirmText="Do you really want to delete?" Enabled="True" 
                                    TargetControlID="lnk_phq_user_del">
                                </asp:ConfirmButtonExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btn_phq_user_add" runat="server" 
                    onclick="btn_phq_user_add_Click" Text="Add PHQ User" />
            </td>
        </tr>
    </table>
</asp:Content>
