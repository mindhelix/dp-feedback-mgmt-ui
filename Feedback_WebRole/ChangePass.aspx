﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ChangePass.aspx.cs" Inherits="Feedback_WebRole.ChangePass" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Change Password</h2>
    <table class="style2">
        <tr>
            <td class="style3">
                <asp:Label ID="Label1" runat="server" Text="Current Password:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_admin_old_pass" runat="server" TextMode="Password"></asp:TextBox>
            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ErrorMessage="This field is mandatory" 
                    ControlToValidate="txt_admin_old_pass" ForeColor="White" 
                    ValidationGroup="admin_pass">*</asp:RequiredFieldValidator>
                <asp:ValidatorCalloutExtender ID="RequiredFieldValidator1_ValidatorCalloutExtender" 
                    runat="server" Enabled="True" TargetControlID="RequiredFieldValidator1">
                </asp:ValidatorCalloutExtender>
            </td>
        </tr>
        <tr>
            <td class="style3">
                <asp:Label ID="Label2" runat="server" Text="New Password:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_admin_new_pass" runat="server" TextMode="Password"></asp:TextBox>
            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ErrorMessage="This field is mandatory" 
                    ControlToValidate="txt_admin_new_pass" ForeColor="White" 
                    ValidationGroup="admin_pass">*</asp:RequiredFieldValidator>
                <asp:ValidatorCalloutExtender ID="RequiredFieldValidator2_ValidatorCalloutExtender" 
                    runat="server" Enabled="True" TargetControlID="RequiredFieldValidator2">
                </asp:ValidatorCalloutExtender>
            </td>
        </tr>
        <tr>
            <td class="style3">
                <asp:Label ID="Label3" runat="server" Text="Confirm New Password:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_admin_new_pass_conf" runat="server" TextMode="Password"></asp:TextBox>
            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ErrorMessage="This field is mandatory" 
                    ControlToValidate="txt_admin_new_pass_conf" ForeColor="White" 
                    ValidationGroup="admin_pass">*</asp:RequiredFieldValidator>
                <asp:ValidatorCalloutExtender ID="RequiredFieldValidator3_ValidatorCalloutExtender" 
                    runat="server" Enabled="True" TargetControlID="RequiredFieldValidator3">
                </asp:ValidatorCalloutExtender>
            &nbsp;<asp:CompareValidator ID="CompareValidator1" runat="server" 
                    ControlToCompare="txt_admin_new_pass" 
                    ControlToValidate="txt_admin_new_pass_conf" 
                    ErrorMessage="Should be same as the New Password field" ForeColor="White" 
                    ValidationGroup="admin_pass">*</asp:CompareValidator>
                <asp:ValidatorCalloutExtender ID="CompareValidator1_ValidatorCalloutExtender" 
                    runat="server" Enabled="True" TargetControlID="CompareValidator1">
                </asp:ValidatorCalloutExtender>
            </td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_admin_pass_change" runat="server" 
                    onclick="btn_admin_pass_change_Click" Text="Change Password" 
                    ValidationGroup="admin_pass" />
&nbsp;<asp:Label ID="lbl_admin_pass_change_rsp" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
