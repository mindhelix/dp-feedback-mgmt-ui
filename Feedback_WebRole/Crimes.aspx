﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Crimes.aspx.cs" Inherits="Feedback_WebRole.Crimes" MaintainScrollPositionOnPostback="true" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Crime Details</h2>
    <table class="style1">
        <tr>
            <td align="right">
    <asp:TextBox ID="txt_search_Cr" runat="server"></asp:TextBox>
                <asp:TextBoxWatermarkExtender ID="txt_search_Cr_TextBoxWatermarkExtender" 
                    runat="server" Enabled="True" TargetControlID="txt_search_Cr" 
                    WatermarkText="Search PS ID">
                </asp:TextBoxWatermarkExtender>
&nbsp;<asp:Button ID="btn_go_Cr" runat="server" onclick="btn_go_Cr_Click" 
        Text="Go" />
&nbsp; <asp:Button ID="btn_revert_Cr" runat="server" onclick="btn_revert_Cr_Click" 
        Text="Revert View" />
            </td>
        </tr>
        <tr>
            <td>
        <asp:GridView ID="grid_crimes" runat="server" CellPadding="4" 
            ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" 
            onselectedindexchanged="grid_crimes_SelectedIndexChanged" ShowFooter="True" 
                    DataKeyNames="PartitionKey,RowKey">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:TemplateField>
                    <FooterTemplate>
                        <asp:Button ID="btn_del_crime" runat="server" onclick="btn_del_crime_Click" 
                            Text="Delete" />
                    </FooterTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chk_del_crime" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="PartitionKey" HeaderText="PartitionKey" 
                    Visible="False" />
                <asp:BoundField DataField="RowKey" HeaderText="RowKey" Visible="False" />
                <asp:BoundField DataField="ps_id" HeaderText="Police Station (PS ID)" />
                <asp:BoundField DataField="incident_count" HeaderText="Incident Count" />
                <asp:BoundField DataField="district_id" HeaderText="District ID" />
                <asp:BoundField DataField="crime_head" HeaderText="Crime Head" />
                <asp:CommandField SelectText="Edit" ShowSelectButton="True" />
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
    <asp:Button ID="btn_prev" runat="server" Enabled="False" Text="&lt;&lt; Prev" 
        onclick="btn_prev_Click" />
                <asp:Label ID="lbl_page_stat" runat="server"></asp:Label>
                <asp:Button ID="btn_next" runat="server" Enabled="False" 
        Text="Next &gt;&gt;" onclick="btn_next_Click" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label ID="lbl_upload_crime_resp" runat="server" 
                    Text="Update Crime Details from CSV file:"></asp:Label>
&nbsp;<asp:FileUpload ID="file_crime_csv" runat="server" />
&nbsp;<asp:Button ID="btn_update_crime_csv" runat="server" onclick="btn_update_crime_csv_Click" 
                    Text="Upload" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <p>
        &nbsp;&nbsp;&nbsp;</p>
</asp:Content>
