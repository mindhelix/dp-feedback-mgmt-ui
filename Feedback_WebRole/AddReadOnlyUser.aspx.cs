﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CitizenReports;
using System.Security.Cryptography;
using System.Text;

namespace Feedback_WebRole
{
    public partial class AddReadOnlyUser : System.Web.UI.Page
    {
        UserObjectDataSource userDS = new UserObjectDataSource();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["logged_in"] == null)
                Response.Redirect("Login.aspx", true);
        }


        //Back to User Control
        protected void btn_back2UserControl_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserControl.aspx");
        }


        //Read Only / Law & Order User submit
        protected void btn_create_read_only_user_Click(object sender, EventArgs e)
        {
            //check if user already exists
            if (userDS.doesUserExists(txt_read_only_user.Text))
            {
                lbl_read_only_user_rsp.Text = "The username already exists! Try a different username.";
                return;
            }

            UserObject userObj = new UserObject();
            userObj.username = txt_read_only_user.Text;

            //Compute MD5 of password
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5 = new MD5CryptoServiceProvider();

            originalBytes = ASCIIEncoding.Default.GetBytes(txt_read_only_pass.Text);
            encodedBytes = md5.ComputeHash(originalBytes);

            userObj.password = BitConverter.ToString(encodedBytes);


            userObj.level = 10; //Read Only user level
            userObj.status = 1; //Active


            //create user
            UserObjectEntity newUserEnt = userDS.CreateUser(userObj);

            txt_read_only_user.Text = "";
            txt_read_only_pass.Text = "";
            lbl_read_only_user_rsp.Text = "DCP user created.";
        }

        //Check username on text change
        protected void txt_read_only_user_TextChanged(object sender, EventArgs e)
        {
            //check if user already exists
            if (userDS.doesUserExists(txt_read_only_user.Text))
            {
                lbl_read_only_user_rsp.Text = "The username already exists! Try a different username.";
                txt_read_only_user.Focus();
            }
            else
            {
                lbl_read_only_user_rsp.Text = "The username available.";
                txt_read_only_pass.Focus();
            }
        }
    }
}