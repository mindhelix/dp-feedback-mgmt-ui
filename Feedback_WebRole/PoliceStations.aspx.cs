﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CitizenReports;

namespace Feedback_WebRole
{
    public partial class PoliceStations : System.Web.UI.Page
    {
        int offset = 0;
        int limit = 10;
        int total_page = 1;
        int page_count = 1;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["logged_in"] == null)
                Response.Redirect("Login.aspx", true);
            if (Session["level"].ToString() != "admin" && Session["level"].ToString() != "phq")
                Response.Redirect("Logout.aspx", true); //NavMenuPhq

            if (Session["level"].ToString() == "phq")
            {
                //Disable Nav
                Menu NavigationMenu = new Menu();
                NavigationMenu = (Menu)Master.FindControl("NavigationMenu");
                NavigationMenu.Visible = false;

                //Enable PHQ Menu
                Menu NavMenuPhq = new Menu();
                NavMenuPhq = (Menu)Master.FindControl("NavMenuPhq");
                NavMenuPhq.Visible = true;
            }

            if (!IsPostBack)
            {
                ViewState["offset"] = offset.ToString();
                ViewState["total_page"] = total_page.ToString();
                ViewState["page_count"] = page_count.ToString();

                //Initialize Grid
                fillGrid();
            }
        }

        //Initialize PoliceStations Grid
        public void fillGrid()
        {
            PSObjectDataSource psDS = new PSObjectDataSource();
            var psObj = psDS.getPoliceStations();

            grid_ps.DataSource = psObj;
            grid_ps.DataBind();


            //Paging ops
            int item_count = psDS.getAllPoliceStations().Count<PSObjectEntity>();
            total_page = item_count / 10;
            if ((item_count % 10) != 0)
                total_page++;

            lbl_page_stat.Text = "Page " + page_count + " of " + total_page;

            if (page_count < total_page)
                btn_next.Enabled = true;


            ViewState["total_page"] = total_page.ToString();
        }

        //Paging Next btn
        protected void btn_next_Click(object sender, EventArgs e)
        {
            btn_prev.Enabled = true;

            offset = Int32.Parse(ViewState["offset"].ToString());
            offset += 10;

            PSObjectDataSource psDS = new PSObjectDataSource();

            grid_ps.DataSource = psDS.getPoliceStations(offset, limit);
            grid_ps.DataBind();
            

            page_count = Int32.Parse(ViewState["page_count"].ToString());
            total_page = Int32.Parse(ViewState["total_page"].ToString());

            lbl_page_stat.Text = "Page " + ++page_count + " of " + total_page;

            ViewState["offset"] = offset.ToString();
            ViewState["page_count"] = page_count.ToString();

            if (page_count == Int32.Parse(ViewState["total_page"].ToString()))
                btn_next.Enabled = false;
        }

        //Paging Prev btn
        protected void btn_prev_Click(object sender, EventArgs e)
        {
            if (btn_next.Enabled == false)
                btn_next.Enabled = true;

            offset = Int32.Parse(ViewState["offset"].ToString());
            offset -= 10;

            PSObjectDataSource psDS = new PSObjectDataSource();

            grid_ps.DataSource = psDS.getPoliceStations(offset, limit);
            grid_ps.DataBind();

            page_count = Int32.Parse(ViewState["page_count"].ToString());
            total_page = Int32.Parse(ViewState["total_page"].ToString());

            lbl_page_stat.Text = "Page " + --page_count + " of " + total_page;

            ViewState["offset"] = offset.ToString();
            ViewState["page_count"] = page_count.ToString();

            if (page_count == 1)
                btn_prev.Enabled = false;
        }


        //Edit Select
        protected void grid_ps_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["PS_PK"] = grid_ps.DataKeys[grid_ps.SelectedIndex].Values[0].ToString();
            Session["PS_RK"] = grid_ps.DataKeys[grid_ps.SelectedIndex].Values[1].ToString();

            Response.Redirect("EditPoliceStation.aspx", true);
        }


        //Delete Police Station
        protected void btn_del_ps_Click(object sender, EventArgs e)
        {

            PSObjectDataSource psDS = new PSObjectDataSource();

            for (int i = 0; i < grid_ps.Rows.Count; i++)
            {
                CheckBox chk_del = new CheckBox();
                chk_del = (CheckBox)grid_ps.Rows[i].Cells[0].FindControl("chk_del_ps");

                if (chk_del.Checked == true)
                {
                    string partitionKey = grid_ps.DataKeys[grid_ps.SelectedIndex].Values[0].ToString();
                    string rowKey = grid_ps.DataKeys[grid_ps.SelectedIndex].Values[1].ToString();

                    //del the entry
                    psDS.delPoliceStation(partitionKey, rowKey);
                    grid_ps.Rows[i].Visible = false;
                }
            }
        }


        //Search
        protected void btn_search_ps_Click(object sender, EventArgs e)
        {
            string ps_id = txt_search_PS.Text;
            searchPS(ps_id);
        }

        //Search Result
        public void searchPS(string ps_id)
        {
            btn_next.Enabled = false;
            btn_prev.Enabled = false;
            lbl_page_stat.Text = "Page 1 of 1";

            PSObjectDataSource psDS = new PSObjectDataSource();
            
            grid_ps.DataSource = psDS.getSearchPS(ps_id);
            grid_ps.DataBind();
        }

        //Revert View
        protected void btn_revert_viewPS_Click(object sender, EventArgs e)
        {
            Response.Redirect("PoliceStations.aspx", true);
        }

        protected void txt_search_PS_TextChanged(object sender, EventArgs e)
        {
            string ps_id = txt_search_PS.Text;
            searchPS(ps_id);
        }

        
    }
}