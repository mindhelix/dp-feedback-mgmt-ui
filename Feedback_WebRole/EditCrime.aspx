﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditCrime.aspx.cs" Inherits="Feedback_WebRole.EditCrime" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 300px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Edit Crime Details</h2>
    <table class="style1">
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="PS ID"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="drop_ps_list" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Incident Count"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_incident_count" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="District ID"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_district_id" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Crime Head"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_crime_head" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_update_crime" runat="server" 
                    onclick="btn_update_crime_Click" Text="Update" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_crime_response" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <p>
        <asp:Button ID="btn_backToCrime" runat="server" Text="&lt;&lt; Back" 
            onclick="btn_backToCrime_Click" /></p>
</asp:Content>
