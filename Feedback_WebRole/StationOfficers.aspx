﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StationOfficers.aspx.cs" Inherits="Feedback_WebRole.StationOfficers" MaintainScrollPositionOnPostback="true" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>

    <script type="text/javascript">
        function CancelClick() {
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Station Officers</h2>

    <asp:Panel ID="Panel1" runat="server" Width="95%">
        <table class="style1">
            <tr>
                <td align="left">
                    <asp:Button ID="btn_add_sho" runat="server" Text="Add SHO" 
                        onclick="btn_add_sho_Click" />
                </td>
                <td align="right">
                    <asp:TextBox ID="txt_search_shoid" runat="server"></asp:TextBox>
                    <asp:TextBoxWatermarkExtender ID="txt_search_shoid_TextBoxWatermarkExtender" 
                        runat="server" Enabled="True" TargetControlID="txt_search_shoid" 
                        WatermarkText="Search SHO ID">
                    </asp:TextBoxWatermarkExtender>
                    <asp:Button ID="btn_shoid_search" runat="server" 
                        onclick="btn_shoid_search_Click" Text="Go" />
                    &nbsp;<asp:Button ID="btn_sho_revert_view" runat="server" 
                        onclick="btn_sho_revert_view_Click" Text="Revert View" />
                </td>
            </tr>
        </table>
     </asp:Panel>
    
    <asp:Panel ID="Panel2" runat="server">
    
    <asp:ListView ID="list_officers" runat="server" 
        onselectedindexchanging="list_officers_SelectedIndexChanging" 
        onitemdeleting="list_officers_ItemDeleting">

        <LayoutTemplate>
            <ul class="officers_list">
                <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
            </ul>
        </LayoutTemplate>

        <ItemTemplate>
            <li class="officers_list_item">
                <asp:Image ID="img_sho_photo" runat="server" ImageUrl="Images/empty-profile.png" />
                <div>
                    <p>
                        <asp:Label ID="Label1" runat="server" Text="SHO ID: "></asp:Label>
                        <asp:Label ID="lbl_sho_id" runat="server" Text='<%#Eval("id") %>'></asp:Label></p>
                    <p>
                        <asp:Label ID="Label2" runat="server" Text="SHO Name: "></asp:Label>
                        <asp:Label ID="lbl_sho_name" runat="server" Text='<%#Eval("name") %>'></asp:Label></p>    
                    <p>
                        <asp:LinkButton ID="lnk_sho_select" Text="Edit" CommandName="Select" runat="server" /> &nbsp; 
                        <asp:LinkButton ID="lnk_sho_delete" Text="Delete" CommandName="Delete" runat="server" />
                        <asp:ConfirmButtonExtender ID="cbe" runat="server"
    TargetControlID="lnk_sho_delete"
    ConfirmText="Are you sure you want to delete?"
    OnClientCancel="CancelClick" />
                        <asp:HiddenField ID="hidden_officer_pk" runat="server" Value='<%#Eval("PartitionKey") %>' />
                    </p>
                </div>
            </li>
        </ItemTemplate>

        <EmptyDataTemplate>
            <div>
                No Officer Details added yet.
            </div>
        </EmptyDataTemplate>

    </asp:ListView>
    </asp:Panel>
    <asp:Panel ID="panel_paging" runat="server" HorizontalAlign="Center">
        <asp:Button ID="btn_prev" runat="server" Text="<< Prev" Enabled="False" 
            onclick="btn_prev_Click" /> 
        &nbsp;<asp:Label ID="lbl_page_stat" runat="server" Text="Page 1 of 1"></asp:Label>  
        &nbsp;<asp:Button ID="btn_next" runat="server" Text="Next >>" 
            onclick="btn_next_Click" />
    </asp:Panel>
    </asp:Content>
