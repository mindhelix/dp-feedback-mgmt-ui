﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using CitizenReports;

namespace Feedback_WebRole
{
    public partial class ChangePass : System.Web.UI.Page
    {
        UserObjectDataSource userDS = new UserObjectDataSource();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["logged_in"] == null)
                Response.Redirect("Login.aspx", true);
        }


        //Change Admin Pass
        protected void btn_admin_pass_change_Click(object sender, EventArgs e)
        {
            //Old Passwd to md5
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5 = new MD5CryptoServiceProvider();

            originalBytes = ASCIIEncoding.Default.GetBytes(txt_admin_old_pass.Text);
            encodedBytes = md5.ComputeHash(originalBytes);

            string md5_old_passwd = BitConverter.ToString(encodedBytes);

            //get UserName
            string Username = userDS.getUsername(Session["user_id"].ToString());

            var userObj = userDS.checkUser(Username, md5_old_passwd);

            if (userObj != null) //Update Password
            {
                //Compute MD5 of new password
                originalBytes = ASCIIEncoding.Default.GetBytes(txt_admin_new_pass.Text);
                encodedBytes = md5.ComputeHash(originalBytes);

                var new_encrypted_pass = BitConverter.ToString(encodedBytes);

                userDS.UpdatePassword(userObj.RowKey, new_encrypted_pass);

                lbl_admin_pass_change_rsp.Text = "Password updated successfully.";
            }
            else
            {
                lbl_admin_pass_change_rsp.Text = "Current password is invalid!";
            }


            lbl_admin_pass_change_rsp.Focus();
        }
    }
}