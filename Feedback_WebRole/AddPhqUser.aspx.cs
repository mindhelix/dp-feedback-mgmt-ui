﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CitizenReports;
using System.Security.Cryptography;
using System.Text;

namespace Feedback_WebRole
{
    public partial class AddPhqUser : System.Web.UI.Page
    {
        UserObjectDataSource userDS = new UserObjectDataSource();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["logged_in"] == null)
                Response.Redirect("Login.aspx", true);
        }

        //Back to User Control
        protected void btn_back2UCfromPHQ_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserControl.aspx");
        }


        //Add PHQ User
        protected void btn_phq_create_Click(object sender, EventArgs e)
        {
            //check if user already exists
            if (userDS.doesUserExists(txt_add_phq_user.Text))
            {
                lbl_add_phq_user_rsp.Text = "The username already exists! Try a different username.";
                return;
            }

            UserObject userObj = new UserObject();
            userObj.username = txt_add_phq_user.Text;

            //Compute MD5 of password
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5 = new MD5CryptoServiceProvider();

            originalBytes = ASCIIEncoding.Default.GetBytes(txt_add_phq_pass.Text);
            encodedBytes = md5.ComputeHash(originalBytes);

            userObj.password = BitConverter.ToString(encodedBytes);


            userObj.level = 4; //PHQ user level
            userObj.status = 1; //Active


            //create user
            UserObjectEntity newUserEnt = userDS.CreateUser(userObj);

            txt_add_phq_user.Text = "";
            txt_add_phq_pass.Text = "";
            lbl_add_phq_user_rsp.Text = "PHQ user created.";
        }


        //Check Username available on change event
        protected void txt_add_phq_user_TextChanged(object sender, EventArgs e)
        {
            //check if user already exists
            if (userDS.doesUserExists(txt_add_phq_user.Text))
            {
                lbl_add_phq_user_rsp.Text = "The username already exists! Try a different username.";
                txt_add_phq_user.Focus();
            }
            else
            {
                lbl_add_phq_user_rsp.Text = "The username available.";
                txt_add_phq_pass.Focus();
            }
        }
    }
}