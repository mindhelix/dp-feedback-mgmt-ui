﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddDcpUser.aspx.cs" Inherits="Feedback_WebRole.AddDcpUser" MaintainScrollPositionOnPostback="true" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
    .style1
    {
        width: 476px;
    }
        .style2
        {
            width: 90px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Add DCP User</h2>
    

<table class="style1">
    <tr>
        <td class="style2">
            <asp:Label ID="Label1" runat="server" Text="Username:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txt_dcp_user" runat="server" AutoPostBack="True" 
                ontextchanged="txt_dcp_user_TextChanged"></asp:TextBox>
        &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ControlToValidate="txt_dcp_user" ErrorMessage="This field is mandatory" 
                ForeColor="White" ValidationGroup="dcp_user">*</asp:RequiredFieldValidator>
            <asp:ValidatorCalloutExtender ID="RequiredFieldValidator1_ValidatorCalloutExtender" 
                runat="server" Enabled="True" TargetControlID="RequiredFieldValidator1">
            </asp:ValidatorCalloutExtender>
        </td>
    </tr>
    <tr>
        <td class="style2">
            <asp:Label ID="Label2" runat="server" Text="Password:"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="txt_dcp_pass" runat="server"></asp:TextBox>
        &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                ControlToValidate="txt_dcp_pass" ErrorMessage="This field is mandatory" 
                ForeColor="White" ValidationGroup="dcp_user">*</asp:RequiredFieldValidator>
            <asp:ValidatorCalloutExtender ID="RequiredFieldValidator2_ValidatorCalloutExtender" 
                runat="server" Enabled="True" TargetControlID="RequiredFieldValidator2">
            </asp:ValidatorCalloutExtender>
        </td>
    </tr>
    <tr>
        <td class="style2">
            <asp:Label ID="Label3" runat="server" Text="District:"></asp:Label>
        </td>
        <td>
            <asp:DropDownList ID="drop_add_dcp_district" runat="server" 
                DataSourceID="ObjectDataSource1" DataTextField="name" DataValueField="id">
            </asp:DropDownList>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                SelectMethod="getAllDistricts" TypeName="CitizenReports.DistrictDataSource">
            </asp:ObjectDataSource>
        </td>
    </tr>
    <tr>
        <td class="style2">
            &nbsp;</td>
        <td>
            <asp:Button ID="btn_add_dcp_submit" runat="server" Text="Save" 
                onclick="btn_add_dcp_submit_Click" ValidationGroup="dcp_user" />
        </td>
    </tr>
    <tr>
        <td class="style2">
            <asp:Button ID="btn_add_dcp_back" runat="server" Text="&lt;&lt; Back" 
                onclick="btn_add_dcp_back_Click" />
        </td>
        <td>
            <asp:Label ID="lbl_add_dcp_rsp" runat="server"></asp:Label>
        </td>
    </tr>
</table>
    

</asp:Content>
