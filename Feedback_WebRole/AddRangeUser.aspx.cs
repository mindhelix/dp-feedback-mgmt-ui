﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CitizenReports;
using System.Security.Cryptography;
using System.Text;

namespace Feedback_WebRole
{
    public partial class AddRangeUser : System.Web.UI.Page
    {
        UserObjectDataSource userDS = new UserObjectDataSource();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["logged_in"] == null)
                Response.Redirect("Login.aspx", true);
        }

        //Back to User Control page
        protected void btn_back2rangeUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserControl.aspx");
        }


        //Create Range User
        protected void btn_create_range_user_Click(object sender, EventArgs e)
        {
            //check if user already exists
            if (userDS.doesUserExists(txt_range_user.Text))
            {
                lbl_add_range_users .Text = "The username already exists! Try a different username.";
                return;
            }

            UserObject userObj = new UserObject();
            userObj.username = txt_range_user.Text;

            //Compute MD5 of password
            Byte[] originalBytes;
            Byte[] encodedBytes;
            MD5 md5 = new MD5CryptoServiceProvider();

            originalBytes = ASCIIEncoding.Default.GetBytes(txt_range_pass.Text);
            encodedBytes = md5.ComputeHash(originalBytes);

            userObj.password = BitConverter.ToString(encodedBytes);


            userObj.level = 3; //Range User level
            userObj.status = 1; //Active


            //create user
            UserObjectEntity newUserEnt = userDS.CreateUser(userObj);

            //add user data
            //add User's range
            UserDataObject userData = new UserDataObject();
            userData.user_id = newUserEnt.RowKey;
            userData.data_key = "Range";
            userData.data_value = drop_add_range.SelectedItem.Value;

            //add to storage
            UserDataDataSource userDataDS = new UserDataDataSource();
            userDataDS.AddUserData(userData);

            txt_range_user.Text = "";
            txt_range_pass.Text = "";
            lbl_add_range_users.Text = "Range user created.";
        }

        //Check user name on text change
        protected void txt_range_user_TextChanged(object sender, EventArgs e)
        {
            //check if user already exists
            if (userDS.doesUserExists(txt_range_user.Text))
            {
                lbl_add_range_users.Text = "The username already exists! Try a different username.";
                txt_range_user.Focus();
            }
            else
            {
                lbl_add_range_users.Text = "The username available.";
                txt_range_pass.Focus();
            }
        }
    }
}