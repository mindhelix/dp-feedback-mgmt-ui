﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddRangeUser.aspx.cs" Inherits="Feedback_WebRole.AddRangeUser" MaintainScrollPositionOnPostback="true" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 450px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Add Range User</h2>
    <table class="style1">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Username:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_range_user" runat="server" AutoPostBack="True" 
                    ontextchanged="txt_range_user_TextChanged"></asp:TextBox>
            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txt_range_user" ErrorMessage="This field is mandatory" 
                    ForeColor="White" ValidationGroup="range_user">*</asp:RequiredFieldValidator>
                <asp:ValidatorCalloutExtender ID="RequiredFieldValidator1_ValidatorCalloutExtender" 
                    runat="server" Enabled="True" TargetControlID="RequiredFieldValidator1">
                </asp:ValidatorCalloutExtender>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Password:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txt_range_pass" runat="server"></asp:TextBox>
            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="txt_range_pass" ErrorMessage="This field is mandatory" 
                    ForeColor="White" ValidationGroup="range_user">*</asp:RequiredFieldValidator>
                <asp:ValidatorCalloutExtender ID="RequiredFieldValidator2_ValidatorCalloutExtender" 
                    runat="server" Enabled="True" TargetControlID="RequiredFieldValidator2">
                </asp:ValidatorCalloutExtender>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Range:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="drop_add_range" runat="server" 
                    DataSourceID="ObjectDataSource1" DataTextField="districtrange" 
                    DataValueField="PartitionKey">
                </asp:DropDownList>
                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                    SelectMethod="getAllRanges" TypeName="CitizenReports.DistrictDataSource">
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Button ID="btn_create_range_user" runat="server" Text="Save" 
                    onclick="btn_create_range_user_Click" ValidationGroup="range_user" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btn_back2rangeUsers" runat="server" 
                    onclick="btn_back2rangeUsers_Click" Text="&lt;&lt; Back" />
            </td>
            <td>
                <asp:Label ID="lbl_add_range_users" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
